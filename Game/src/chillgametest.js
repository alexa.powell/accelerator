
if (!window.chillgametest) {
    window.chillgametest = function() {

        this.resize = function() {
            let width;
            let height;
            
            if (window.innerHeight > window.innerWidth){
                let ratio = 1334/750;
                width = window.innerWidth;
                height = width*ratio;
                console.log("portrait screen");
                return 'portrait';
            }
            else{
                let ratio = 1.618;
                /*canvas.style.marginBottom = "50px";*/
                /*canvas.style.marginTop = "50px";*/
                height = window.innerHeight - 100;
                width = height/ratio;
                console.log("landscape screen");
                return 'landscape';
            }
        }
        
        this.sinPattern = function(width_stretch, height_stretch, number_of_balls) {
            coord_array = [];
            let canvas = {
                width : 100,
                height : 200
            };
            for (var i = 0; i < number_of_balls; i++){
        
                let pos_ninety_deg = Math.PI/2;
                let neg_ninety_deg = -Math.PI/2;
        
                let x_pos = width_stretch*Math.sin(pos_ninety_deg/4*i)+canvas.width/2;
                coord_array.push({x : x_pos, y : -i*height_stretch, status : 1});
            }
            return coord_array;
        }
    }
}