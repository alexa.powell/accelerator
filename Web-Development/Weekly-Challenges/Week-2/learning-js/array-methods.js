const people = [
    {first: 'alexa', last: 'powell', hairColour: 'blonde'},
    {first: 'liam', last: 'scott', hairColour: 'brown'},
    {first: 'anna', last: 'holland-smith', hairColour: 'blonde'},
];
console.table(people);

const blondes = people.filter(function(person) { // filter loops through the array according to this function
    if (person.hairColour ==='blonde') {
        return true; // keeps through the filter, everything else defaults to false and is filtered out
    }
});
console.table(blondes);

const blondesWithArrowFunc = people.filter(person => person.hairColour ==='blonde');
console.table(blondesWithArrowFunc);

// `${'Hello,'} ${'World}` outputs Hello, World, i.e. it puts things together with spaces between them.

const fullNames = people.map(people => `${people.first} ${people.last}`);
console.log(fullNames);