package com.thg.exercises;

import static org.junit.Assert.assertTrue;
import java.util.Arrays;
import static org.junit.Assert.*;

import org.junit.Test;

public class PalindromeTest {

    @Test
    public void checkOneDigit()
    {
        palindrome pal = new palindrome(1);
        boolean oneDigit = pal.getAns();
        assertTrue("Should know that one digit is a palindrome", oneDigit);
    }

    @Test
    public void checkTwoDigitsPass()
    {
        palindrome pal = new palindrome(11);
        boolean oneDigit = pal.getAns();
        assertTrue("Should correctly spot 2 digit palindrome", oneDigit);
    }

    @Test
    public void checkTwoDigitsFail()
    {
        palindrome pal = new palindrome(12);
        boolean oneDigit = pal.getAns();
        assertFalse("Should correctly spot 2 digit non-palindrome", oneDigit);
    }

    @Test
    public void checkOddDigitsPass()
    {
        palindrome pal = new palindrome(12321);
        boolean oneDigit = pal.getAns();
        assertTrue("Should correctly spot palindrome with an odd number of digits", oneDigit);
    }

    @Test
    public void checkOddDigitsFail()
    {
        palindrome pal = new palindrome(12345);
        boolean oneDigit = pal.getAns();
        assertFalse("Should correctly spot non-palindrome with an odd number of digits", oneDigit);
    }

    @Test
    public void checkEvenDigitsPass()
    {
        palindrome pal = new palindrome(123321);
        boolean oneDigit = pal.getAns();
        assertTrue("Should correctly spot palindrome with an even number of digits", oneDigit);
    }

    @Test
    public void checkEvenDigitsFail()
    {
        palindrome pal = new palindrome(123456);
        boolean oneDigit = pal.getAns();
        assertFalse("Should correctly spot non-palindrome with an even number of digits", oneDigit);
    }
}
