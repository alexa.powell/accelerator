#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 17 09:55:02 2018

@author: powella
"""

# input all the info provided in the question
portion_down_payment = 0.25
current_savings = 0.0
r = 0.04

# get user input
total_cost = int(input('Enter the cost of your dream house: '))
portion_saved = float(input('Enter what percentage of your salary you would like to save (as a decimal): '))
annual_salary = int(input('Enter your annual salary: '))

# calculate necessary values
monthly_salary = annual_salary/12
money_needed = total_cost*portion_down_payment
putting_away = monthly_salary*portion_saved

i = 0
while current_savings < money_needed :
    current_savings += current_savings*r/12 + putting_away
    i += 1

#print('it will take you', i, 'months to save up for your dream house')

years = int(i/12)
months = i%12

if years != 0 and months != 0:
    print('\nIt will take you', years, 'years and', months, 'months to save up for your dream house')
elif years == 0:
    print('\nIt will take you', months, 'months to save up for your dream house')
else:
    print('\nIt will take you', years, 'years to save up for your dream house')
        
