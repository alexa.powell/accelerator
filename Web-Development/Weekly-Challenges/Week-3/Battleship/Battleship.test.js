const Ship = require('./Battleship');

describe('testing Ship factory function', () => {
   let ship; 
   beforeAll(() => {
        ship = Ship(5);
    });

    test('ship should be an object', () => {
        expect(ship).toBeDefined();
    });

    test('ship should contain length', () => {
        expect(ship.length).toEqual(5);
    });

    test('ship should contain hit', () => {
        expect(ship.length).toBeDefined();
    });

    test('ship should set up hits array with correct values', () => {
        expect(ship.hits[0]).toBeFalsy();
        expect(ship.hits[1]).toBeFalsy();
        expect(ship.hits[2]).toBeFalsy();
        expect(ship.hits[3]).toBeFalsy();
        expect(ship.hits[4]).toBeFalsy();
    });
    test('hits array should be right length', () => {
        expect(ship.hits.length).toEqual(5);
    })
    
    test('hit() should update hits array correctly', () => {
        expect(ship.hits[0]).toBeFalsy();
        ship.hit(0);
        expect(ship.hits[0]).toBeTruthy();
    });

    test('isSunk() should work correctly', () => {
        expect(ship.isSunk()).toBeFalsy();
        ship.hit(1);
        ship.hit(2);
        ship.hit(3);
        ship.hit(4);
        expect(ship.isSunk()).toBeTruthy();
    })
});