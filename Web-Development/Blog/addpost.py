#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 25 14:02:39 2018

@author
"""

def add_new_posts(title):
    blog_dict = {}

    names_open = open("names.txt", "r")
    names_read = names_open.read()
    names = list(names_read.strip().split("---"))
    del names[-1]

    posts_open = open("posts.txt", "r")
    posts_read = posts_open.read()
    posts = list(posts_read.strip().split("---"))
    del posts[-1]

    for i in range(len(names)):
        blog_dict[names[i]] = posts[i]

    return blog_dict[title]

#print(add_new_posts('alexa'))


def login(username, password):
    blog_dict = {}

    names_open = open("username.txt", "r")
    names_read = names_open.read()
    names = list(names_read.strip().split("---"))
    del names[-1]

    posts_open = open("passwords.txt", "r")
    posts_read = posts_open.read()
    posts = list(posts_read.strip().split("---"))
    del posts[-1]

    for i in range(len(names)):
        blog_dict[names[i]] = posts[i]

    if blog_dict[username] == password:
        return True
    else:
        return False


def addMembers():
    members_open = open("members.txt", "r")
    members_read = int(members_open.read())
    members_open.close()
    members_read += 1
    members_read = str(members_read)
    members_open = open("members.txt", "w")
    members_open.write(members_read)
    members_open.close()

addMembers()