#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 28 16:08:38 2018

@author: powella
"""

"""This code is me figuring out how list(map(...)) works and taking 3 items as input from the user,
then comparing the three lists item by item and giving back a score on who won, where winning """

alice=input("Alice's score - enter 3 integers separated by a comma and a space between 1 and 100: ")
alice=list(map(int, alice.split(", "))) 

# print('\nThe length of the list \'alice\' is', len(alice))
# print('\nThe middle entry in this string is', alice[1])

bob=input("Bob's score - enter 3 integers separated by a space between 1 and 100: ")
bob=list(map(int, bob.split()))

# print('\nThe object \'bob\'is of type', type(bob), '\n')

def compareTriplets(a, b):
    score = [0,0]
    for i in range(3):
        if a[i]>b[i]:
            score[0]+=1
            #print('score at', i, 'is', score)
        elif a[i]<b[i]:
            score[1]+=1
            #print('score at', i, 'is', score)
        #else:
            #print('score at', i, 'is', score)
    return score

def main():
    result = compareTriplets(alice, bob)
    print('The final score of alice to bob is', result[0], ':', result[1])
    if result[0]>result[1]:
        print('Alice wins!')
    elif result[0]<result[1]:
        print('Bob wins!')
    else:
        print('It\'s a draw!')

main()