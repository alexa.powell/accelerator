# Learning React

## Setting up

To set up, type either `npm init react-app name-of-app` or `create-react-app name-of-app` in the parent directory you want to create your app in. This creates a folder called `name-of-app` in which your react app will live.  

To run your app, run `npm start` inside `name-of-app` folder.

## Basics

* Your app will be considered and written component-wise rather than concern-wise, i.e. it will be split up into nav bar, main header, etc. with HTML and JavaScript being mashed up together using JSX. _(Really it is XML not HTML but they are very similar so you can think of it as HTML)_
* **JSX:** JavaScript and XML (like HTML) is the language you use in your JavaScript files to be rendered by React. When writing JavaScript where XML is expected, e.g. in the `return` of a component, put the JavaScript in {curly braces}.
* **State:** State is something to include when the component will change its rendering based on _user interaction with the web page_ (application data). Components can be **stateful** or **stateless** and the state of a stateful component will be mutated through user interactions. Access and set initial state using `this.state.thing`, update using `this.setState()`.
* **Stateful:** A component is stateful if what is being displayed by it depends in some way on application data, for example a counter which increments by one each time the user presses a given button - the number displayed by the counter would depend on how many times the user has clicked the button. This counter would be stateful. Also known as a **class component**. 
* **Stateless:** A component which will simply render on load and remain the same, regardless of application data. This should be the default choice for all components. Also known as a **functional component**, or **dumb**.
* **Props:** These are the properties of a component, passed in as arguments, often by a parent. Props are immutable and do not change through user interactions. State can be passed down to child components through props.
* **Lifecycle method:** A function that is invoked at some point during a component's _lifecycle_, which is the process where a component is created, updated and destroyed. These methods should never be invoked directly but should be invoked by React as the components are used. A component's state cannot be updated until it is _finished rendering for the first time_, i.e. until it is **mounted**, and once it is, the **componentDidMount** method will be invoked.
* **Conditional Rendering:** Part of the user interface which is only displayed if some condition is met, which could be in state or passed in as props. Can use if/else statements, ternary statements - { conditional ? doIfTrue : doIfFalse } or short circuit operator && - { js statement && HTML to be rendered } where HTML is only rendered if the js statement evaluates to true.

## Writing components

When writing a stateful component you want it to be a **class** _extending_ **Component**, with **super()** _in its constructor_ and **return** _inside_ **render()**:

```javascript
import React, { Component } from 'react';

class App extends Component {
    constructor() {
        super();
    };

    takeInput = () => {
        // state-changing logic, say to take input of user's name
    }

    render() {
        return (
            <div> Hello, {this.takeInput} </div>
        );
    };
```

When you are writing a stateless component you simply write a function with _no render()_ like so:

```javascript
import React from 'react';

const App = (props) => {
    return (
        <div > Hello, {props.name} </div>
    );
};
```

When inserting one component, e.g. Counter, into another, use the notation `<Counter/>` where you want the component to be inserted, taking care to include `import Counter from './Counter.js';` into the top of wherever it is used, and `export default Counter;` at the bottom of wherever it is defined. 

Stack overflow discussion on state and props: https://stackoverflow.com/questions/27991366/what-is-the-difference-between-state-and-props-in-react

## Setting and updating state

When setting state, it is important to use `setState()` as mutating the state directly through `this.state.attribute = value` can lead to bugs and hard-to-optimise components, with optimised components potentially not re-rendering.  

State is given as an object, so you can access individual attributes to update them inside `setState()`: 

```javascript
this.state = {count: 0};
this.setState({ count: this.state.count + 1 });
```

You can also use an arrow function to do this:

```javascript 
this.state = {count: 0};
this.setState((prevState) => 
    ({ count: prevState.count + 1 })    // note the parentheses around the curly braces
);
```
where this would increment the count by one. **Don't forget** to put parentheses around the curly braces when using the arrow function in this way!

### Optional second argument

When using `setState()` it is important to remember that it is **asynchronous**, that is the code will not wait for this function to finish before moving on, it can execute concurrently. This means that if, for example, you update the state of a component and then call a function which utilises state, that function may not be using the most recent state:

```javascript
class App extends Component {
    this.state = {
        age: 22,
    }

    handleChange = (event) => {
        console.log(this.state.age)
        this.setState({ age: 23 })
        console.log(this.state.age)
    };
}
```
This would log `22` followed by `22` as the state has not been updated by the time `console.log` is called again. The way to avoid this is to include the function that depends on state, in our case simply logging the age, as a **second argument** in the `setState()` function:

```javascript
class App extends Component {
    this.state = {
        age: 22,
    }

    handleChange = (event) => {
        console.log(this.state.age)
        this.setState({ age: 23 }, () => {console.log(this.state.age)})
    };
}
```
This time we would get `22` followed by `23` in the console, since the second argument in `setState()` will _only_ be called when the state has been updated, side-stepping the issue of asynchronicity.

### Making copies for setting state

In practise a state's attribute will often be an array of objects, so when updating these you should make a copy of the array, change whichever attribute you need to and then return that new array, as opposed to mutating the original array. Take care when making copies that you aren't simply creating a new reference to the same value. Here are a few ways to make copies of things:

* When working with objects, you can use a _spread operator_ to 'spread' out all the values from one object to another: `b = {...a}`. This also works to merge two objects together to create a third: `c = {...a, ...b}`. This **does not work with nested objects** as they still contain only pointers will be copied across, meaning updating one will update both.
* You can also use `b = Object.assign({}, a)` to achieve the  same as in the last bullet point, however this method has the same pitfall, it only works with objects with one level of depth - it is a _shallow copy_.
* **Best:** You can use `let b = JSON.parse(JSON.stringify(a))` to make a **deep clone** of object a called b, where any changes made to a after this will not affect b. `JSON.parse()` turns a JSON object into a JavaScript one, and `JSON.stringify()` turns a JavaScript object into a JSON one. 
* For arrays, use array functions like map, filter, reduce which all return a new array and allow you to modify values if necessary, e.g. `b = a.map((elem) => elem + 2)`. This is also a shallow copy however, so does not work with nested arrays. Use `JSON.parse(JSON.stringify())` to overcome this again.

##