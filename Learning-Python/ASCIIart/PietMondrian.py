red = u"\u001b[48;5;196m"
Red = [red, red, red]
RED = " ".join(Red)
blue = u"\u001b[48;5;21m"
Blue = [blue, blue, blue]
BLUE = " ".join(Blue)
yellow = u"\u001b[48;5;226m"
Yellow = [yellow, yellow, yellow]
YELLOW = " ".join(Yellow)
black = u"\u001b[48;5;232m"
Black = [black, black, black]
BLACK = " ".join(Black)
white = u"\u001b[48;5;231m"
White = [white, white, white]
WHITE = " ".join(White)

def print_colour(colour, units):
    line = []
    for i in range(units*3):
        line.append(colour)
    Line = " ".join(line)
    return(Line)

# top black line
print(print_colour(black, 42), u"\u001b[0m")

# section 1
for i in range(12):
    print(BLACK, print_colour(red, 24), BLACK, print_colour(yellow, 15), BLACK, u"\u001b[0m")

# line between Y and W_1, W_2
print(BLACK, print_colour(red, 24), print_colour(black, 17), u"\u001b[0m")

# section 2
for i in range(11):
    print(BLACK, print_colour(red, 24), BLACK, print_colour(white, 7), BLACK, print_colour(white, 7), BLACK, u"\u001b[0m")

# middle black line
print(print_colour(black, 42), u"\u001b[0m")

# section 3
for i in range(5):
    print(print_colour(black, 14), print_colour(white, 11), BLACK, print_colour(white, 15), BLACK, u"\u001b[0m")

# line between W_3 and W_5
print(print_colour(black, 42), u"\u001b[0m")

# section 4
for i in range(5):
    print(print_colour(black, 14), print_colour(white, 11), BLACK, print_colour(blue, 15), BLACK, u"\u001b[0m")

# line between B_1 and W_6
print(print_colour(black, 26), print_colour(blue, 15), BLACK, u"\u001b[0m")

# section 5
for i in range(3):
    print(BLACK, print_colour(white, 12), print_colour(black, 13), print_colour(blue, 15), BLACK, u"\u001b[0m")

# bottom line
print(print_colour(black, 42), u"\u001b[0m")

print(RED, u"\u001b[0m")
print(red, u"\u001b[0m")
