# Problem Set 4A
# Name: Alexa
# Collaborators:
# Time Spent: x:xx

def get_permutations(sequence):

    if len(sequence) < 2:
        # this executes when the function gets down to a single character which can't be split any further,
        # stopping the downwards recursion and starting the next stage of feeding info back up the recursion
        one_character_list = [sequence] # must have a list
        return one_character_list

    else:
        first_character = sequence[:1]
        rest_of_word = get_permutations(sequence[1:])
        # now we permute first_character through every position in every string in the list rest_of_word
        new_permutations = []
        for permutation in rest_of_word:
            for i in range(len(permutation) + 1):
                # this range is because there are n+1 positions for a character to be added into a string of length n
                new_perm = permutation[:i]+first_character+permutation[i:]
                new_permutations.append(new_perm)
                # this puts the character in each possible position and appends the result to a list to be returned

        return new_permutations
        # return permute(first, rest)


if __name__ == '__main__':

    print('\nRunning tests on the get_permutations function')

    test_1_expected = ['abc', 'acb', 'bac', 'bca', 'cab', 'cba']
    test_1_actual = get_permutations('abc')
    pass_1 = True
    for permutation in test_1_expected:
        if permutation not in test_1_actual:
            pass_1 = False
    if len(test_1_actual) != len(test_1_expected):
        pass_1 = False
    
    if pass_1:
        print('\n1: PASS: get_permutations')
    else:
        print('\n1: FAIL: get_permutations')

    test_2_expected = ['def', 'dfe', 'edf', 'efd', 'fde', 'fed']
    test_2_actual = get_permutations('def')

    pass_2 = True
    for permutation in test_2_expected:
        if permutation not in test_2_actual:
            pass_2 = False
    if len(test_2_actual) != len(test_2_expected):
        pass_2 = False
    
    if pass_2:
        print('\n2: PASS: get_permutations')
    else:
        print('\n2: FAIL: get_permutations')

    print('\nFinished running tests\n')

#    #EXAMPLE
#    example_input = 'abc'
#    print('Input:', example_input)
#    print('Expected Output:', ['abc', 'acb', 'bac', 'bca', 'cab', 'cba'])
#    print('Actual Output:', get_permutations(example_input))
    
#    # Put three example test cases here (for your sanity, limit your inputs
#    to be three characters or fewer as you will have n! permutations for a 
#    sequence of length n)

