
if (!window.NumberList) {
    window.NumberList = function() {
        const numbers = [];

        this.sumAll = function() {
            let sum = 0;
            for (const i=0; i< numbers.length; i++) {
                sum+= numbers[i];
            }
            return sum;
        }

        this.multiplyAll = function() {
            if (numbers.length === 0) {
                return null;
            }
            let product = 1;
            for (const i=0; i< numbers.length; i++) {
                product*= numbers[i];
            }
            return product;
        }
    }
}