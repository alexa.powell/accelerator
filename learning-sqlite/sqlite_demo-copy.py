# using functions to do the things in the sqlite_demo for you so you don't have to type out each individual command

import sqlite3
from employee import Employee # allowed since employee is in same directory

conn = sqlite3.connect(':memory:')

# create a cursor which allows us to execute some sql commands:
c = conn.cursor() # we can now start running sql commands using the execute method

# CREATING A TABLE CALLED employees:

c.execute("""CREATE TABLE employees (
            first text,
            last text,
            pay integer
            )""")

def insert_emp(emp):
    with conn: # means we don't need a commit statement anymore as doing with connection
        c.execute("INSERT INTO employees VALUES (:first, :last, :pay)", {'first': emp.first, 'last': emp.last, 'pay': emp.pay})


def get_emps_by_name(lastname):
    c.execute("SELECT * FROM employees WHERE last=:last", {'last': lastname})
    return c.fetchall() # notice no commit statements needed with SELECT so 'with conn' not needed


def update_pay(emp, pay):
    with conn:
        c.execute("""UPDATE employees SET pay = :pay
                    WHERE first = :first AND last = :last""",
                  {'first': emp.first, 'last': emp.last, 'pay': pay})


def remove_emp(emp):
    with conn:
        c.execute("DELETE from employees WHERE first = :first AND last = :last", {'first': emp.first, 'last': emp.last})


emp_1 = Employee('John', 'Doe', 80000)
emp_2 = Employee('Jane', 'Doe', 80000)

insert_emp(emp_1)
insert_emp(emp_2)

emps = get_emps_by_name('Doe')
print(emps)

update_pay(emp_2, 95000)
remove_emp(emp_1)

emps = get_emps_by_name('Doe')
print(emps)

conn.close()

