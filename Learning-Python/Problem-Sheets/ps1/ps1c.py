#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 17 14:17:04 2018

@author: powella
"""

# input all the info provided in the question
portion_down_payment = 0.25
r = 0.04
total_cost = 1000000

# get user input
annual_salary = int(input('Enter your annual salary: '))

# calculate necessary values
money_needed = total_cost*portion_down_payment

# set up values for bisection
low = 0
high = 10000
mid = int((low+high)/2) #set up initial mid-point

# function to calculate the savings after 3 years based on the savings rate being tested and salary
def calculate_savings(annual_salary, saving_rate):
    
    current_savings = 0.0
    semi_annual_raise = 0.07
    
    for i in range(36):
        
        if i%6 == 0 and i != 0: # adding this in to execute every 6 months but not on the first loop
            annual_salary = annual_salary*(1+semi_annual_raise)
            
        monthly_salary = annual_salary/12
        putting_away = monthly_salary*(saving_rate/10000) # calculates how much you are putting away each month at your current rate
        current_savings += current_savings*r/12 + putting_away
    
    return current_savings

# Boolean statement to prevent later bits of code being executed if not possible to save in 3 years
not_possible = False 

# an if statement to print if the money_needed cannot be achieved in 3 years with 100% saved
if calculate_savings(annual_salary, 10000) < money_needed:
    print('It is not possible to pay the down payment in 3 years with this salary')
    not_possible = True

counter = 1

# implementing bisection:
# recalculate the savings after 3 years each time with a new rate, checking it's possible before running
while abs(calculate_savings(annual_salary, mid)-money_needed) >= 100 and not_possible == False:
           
    if calculate_savings(annual_salary, mid) < money_needed:
        low = mid
    elif calculate_savings(annual_salary, mid) > money_needed:
        high = mid

    mid = int((low+high)/2)
    counter += 1
 
# if statement to only print if possibe to save in 3 years
if not_possible == False:
    print('The desired savings rate is:', mid/10000, '\nSteps in bisection search:', counter)