class GameOfLife(object):

    """Initialise an instance of this class with the size of the grid you would like to see
    along with the coordinates of the cells you want to be alive in the first generation
    given as a list of tuples of the form (x, y)"""

    def __init__(self, grid_size, living_cells):
        self.grid_size = grid_size
        self.initial_number_of_living_cells = len(living_cells)
        self.grid_status = {}
        self.next_gen_grid_status = {}
        for x in range(grid_size):
            for y in range(grid_size):
                self.grid_status[(x, y)] = False
        for cell in living_cells:
            self.grid_status[cell] = True
    
    def displayGrid(self):
        for x in range(self.grid_size):
            for y in range(self.grid_size):
                if y != (self.grid_size - 1):
                    if self.grid_status[(x, y)]:
                        print(1, end = '')
                    else:
                        print(0, end = '')
                else:
                    if self.grid_status[(x,y)]:
                        print(1)
                    else:
                        print(0)

    def findNeighbours(self, cell):
        x = cell[0]
        y = cell[1]
        neighbours = [(x-1, y+1), (x, y+1), (x+1, y+1), (x+1, y), (x+1, y-1), (x, y-1), (x-1, y-1), (x-1, y)]
        # now check if any cells are out of range of the grid and remove them:
        cells_to_be_deleted = []
        for neighbour in neighbours:
            if any([neighbour[0] < 0, neighbour[0] >= self.grid_size, neighbour[1] < 0, neighbour[1] >= self.grid_size]):
                cells_to_be_deleted.append(neighbour)
        for bad_cell in cells_to_be_deleted:
            neighbours.remove(bad_cell)
        # we add to a new list and then remove since if you edit a list while iterating over it you miss elements
        return neighbours

    def nextGenerationStatus(self, cell):
        neighbours = self.findNeighbours(cell)
        number_of_living_neighbours = 0
        for neighbour in neighbours:
            if self.grid_status[neighbour]:
                number_of_living_neighbours += 1

        if self.grid_status[cell]:
            if number_of_living_neighbours in [2, 3]:
                self.next_gen_grid_status[cell] = True
            else:
                self.next_gen_grid_status[cell] = False
        else:
            if number_of_living_neighbours == 3:
                self.next_gen_grid_status[cell] = True
            else:
                self.next_gen_grid_status[cell] = False

    def updateGrid(self):
        for cell in self.grid_status.keys():
            self.nextGenerationStatus(cell)
        self.grid_status = self.next_gen_grid_status.copy()

    def playGame(self):
        number_of_living_cells = self.initial_number_of_living_cells
        generation_counter = 1
        while number_of_living_cells != 0:
            last_generation = self.grid_status.copy()
            print('Generation', generation_counter)
            self.displayGrid()
            self.updateGrid()
            if self.grid_status == last_generation:
                print('The population has a reached constant and unchanging state by generation', generation_counter)
                break
            number_of_living_cells = 0
            for cell in self.grid_status.keys():
                if self.grid_status[cell]:
                    number_of_living_cells += 1
            generation_counter += 1
            
        if number_of_living_cells == 0:
            print('All cells have died')
        print('The simulation has finished')


if __name__ == '__main__':
    test = GameOfLife(4, [(0,0), (1,1), (2,2), (3,3), (0,2), (1,3), (2, 1), (3, 0)])
    test.playGame()