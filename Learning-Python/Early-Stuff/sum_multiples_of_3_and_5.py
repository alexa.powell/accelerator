#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 28 17:40:21 2018

@author: powella
"""

"""Early attempt at writing a function to calculate the sum
of all multiples of 3 and 5"""

multiples = []
for x in range(1000):
    y = x + 1
    if y % 3 == 0:
        multiples.append(y)
        continue
    elif y % 5 == 0 :
        multiples.append(y)
total = sum(multiples)
print(total)
