package com.thg.exercises;

// I have done this exercise with taking an integer as an input and manipulating them as strings,
// I appreciate that simply using strings throughout would have been easier but I wanted to
// get comfortable with manipulating and switching between types in Java

public class palindrome {

    private int numToCheck;
    private int numOfDigits;

    public palindrome(int n) {
        this.numToCheck = n;
        this.numOfDigits = String.valueOf(numToCheck).length();
    }

    private boolean isPalindrome() {

        // while number has more than one digit
        while (numOfDigits > 1) {

            // 2 digits
            if (numOfDigits == 2) {
                String first = String.valueOf(numToCheck).substring(0,1);
                String second = String.valueOf(numToCheck).substring(1,2);

                if (first.equals(second)) {
                    return true;
                }
                else {
                    return false;
                }
            }
            // more than 2 digits
            else {
                String first = String.valueOf(numToCheck).substring(0,1);
                String last = String.valueOf(numToCheck).substring(numOfDigits-1,numOfDigits);

                if (first.equals(last)) {

                    // create new string for recursion with first and last digits removed
                    String newNumString = String.valueOf(numToCheck).substring(1, numOfDigits - 1);
                    int newNumToCheck = Integer.parseInt(newNumString);

                    // update variables
                    this.numToCheck = newNumToCheck;
                    this.numOfDigits = String.valueOf(numToCheck).length();

                    // implement recursion
                    return isPalindrome();
                }
                else {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean getAns() {
        boolean ans = isPalindrome();
//        System.out.println(ans);
        return ans;
    }
}
