# Exercise 1

* Find the IDs of all students who were taught by an instructor named Einstein; make sure there are no duplicates in the result:
~~~~sql
SELECT s.ID
FROM student s, takes ta, teaches te, instructor i
WHERE s.ID = ta.ID AND ta.course_id = te.course_id AND te.ID = i.ID AND i.name = 'Einstein';
~~~~
Try and do with JOINs as this is more efficient:
~~~~sql
SELECT DISTINCT(takes.ID)
FROM takes
JOIN (SELECT t.course_id
      FROM instructor i
      JOIN teaches t
      ON i.ID = t.ID
      WHERE i.name = 'Einstein') Eins
ON takes.course_id = Eins.course_id;
~~~~
Here I have used a JOIN (INNER) on instructor and teaches to find which courses are taught by Einstein, which I specify with a WHERE clause. I take this as a nested query and JOIN it with takes to find the IDs of the students who take a course satisfying the necessary condition. Both of the above queries achieve the same thing just with different efficiency - a JOIN clause allows MySQL to look at both tables at once rather than going through each entry in the tables and comparing them, which is what the WHERE clause does.
* Find all instructors earning the highest salary (there may be more than one with the same salary):
~~~~sql
SELECT name, salary
FROM instructor
JOIN (SELECT MAX(salary) max_sal
      FROM instructor) m
ON salary = m.max_sal;
~~~~
* Find the enrolment of each section that was offered in Autumn 2009:
~~~~sql
SELECT a.name, a.ID, b.course_id, b.sec_id
FROM (SELECT DISTINCT(s.name), s.ID, t.sec_id, t.course_id
      FROM student s
      JOIN takes t
      ON s.ID = t.ID) a
JOIN (SELECT sec_id, course_id
      FROM section
      WHERE semester = 'Fall' AND year = 2009) b
ON a.sec_id = b.sec_id AND a.course_id = b.course_id;
~~~~