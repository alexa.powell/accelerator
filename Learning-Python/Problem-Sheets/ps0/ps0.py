#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 28 17:20:55 2018

@author: powella
"""

import math
xs = input('Enter number x:')
x=int(xs)
ys = input('Enter number y:')
y=int(ys)
print(x, 'to the power', y, '=', x**y)
print('log(x)=', math.log2(x))
