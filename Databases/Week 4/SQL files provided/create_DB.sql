

DROP DATABASE IF EXISTS company_lab;
CREATE DATABASE IF NOT EXISTS company_lab;
USE company_lab;

SELECT 'CREATING DATABASE STRUCTURE' as 'INFO';

DROP TABLE IF EXISTS employee,
                     department,
                     project,
                     works_on,
                     dep_location,
                     dependent;

/*!50503 set default_storage_engine = InnoDB */;
/*!50503 select CONCAT('storage engine: ', @@default_storage_engine) as INFO */;

CREATE TABLE employee (
    NI      INT             NOT NULL,
    bdate  DATE            NOT NULL,
    fname  VARCHAR(14)     NOT NULL,
    lname   VARCHAR(16)     NOT NULL,
    gender      ENUM ('M','F')  NOT NULL,    
    hire_date   DATE            NOT NULL,
     job char(15),
    salary decimal(14,2),
    dno INT NOT NULL,
    superNI INT             NOT NULL,
         PRIMARY KEY (NI)
);

CREATE TABLE department (
    dnum     INT        NOT NULL,
    dname   VARCHAR(40)     NOT NULL,
    mgrNI INT NOT NULL,
    mgrstartdate DATE,
    FOREIGN KEY (mgrNI) REFERENCES employee (NI) ON DELETE CASCADE,
    PRIMARY KEY (dnum),
    UNIQUE  KEY (dname)
);


CREATE TABLE dept_location (
   dno       INT             NOT NULL,
   dlocation      CHAR(10)         NOT NULL,
   FOREIGN KEY (dno)  REFERENCES department (dnum)    ON DELETE CASCADE,
      PRIMARY KEY (dno,dlocation)
); 

CREATE TABLE project (
   pnum     INT             NOT NULL,
    pname     CHAR(15)         NOT NULL,
    dno   INT          NOT NULL,
    plocation    CHAR(10) default 'Man',
    FOREIGN KEY (dno)  REFERENCES department   (dnum)  ON DELETE CASCADE,
    PRIMARY KEY (pnum)
);

CREATE TABLE works_on (
    eNI      INT             NOT NULL,
    pno       INT     NOT NULL,
   hours numeric (4,2),
    FOREIGN KEY (eNI) REFERENCES employee (NI) ON DELETE CASCADE,
    FOREIGN KEY (pno) REFERENCES project (pnum) ON DELETE CASCADE,
    PRIMARY KEY (eNI,pno)
); 

CREATE TABLE dependent (
    eNI     INT             NOT NULL,
   depen_name     char(15)             NOT NULL,
     gender      ENUM ('M','F')  NOT NULL,
    bdate     DATE            NOT NULL,
    relationship char(15),
    FOREIGN KEY (eNI) REFERENCES employee (NI) ON DELETE CASCADE,
    PRIMARY KEY (eNI,depen_name),
    UNIQUE  KEY (depen_name)
); 
ALTER TABLE employee
ADD FOREIGN KEY (superNI) REFERENCES employee (NI) ON DELETE CASCADE,
ADD FOREIGN KEY (dno) REFERENCES department (dnum) ON DELETE CASCADE;



