# https://www.youtube.com/watch?v=pd-0G0MigUA

import sqlite3
from employee import Employee # allowed since employee is in same directory

# connection object that represents our database. Created variable.
conn = sqlite3.connect('employee.db') # this created the file employee.db the first time it ran
# if you want in-memory database, replace 'employee.db' with ':memory:'. This is helpful for testing as you don't need to comment
# out your insert and create statements each time as the memory wipes and the programme runs from scratch each time

# create a cursor which allows us to execute some sql commands:
c = conn.cursor() # we can now start running sql commands using the execute method

# CREATING A TABLE CALLED employees:

# use c. since this is the cursor. ONLY HAVE THIS UNCOMMENTED ONCE - to create table once, will get errors if try to run again
# c.execute("""CREATE TABLE employees (
#             first text,
#             last text,
#             pay integer
#             )""")
# after this bit, run the code once to create the table, won't be able to run again as 'table employees already exists'

# used """ as writing multiple lines, if only need one line can just use "
# first, last and pay are the names of the columns, text and integer are the data types
# other data types are null, real and blob (a blob of data, stored exactly as it was input

# INSERTING ITEMS INTO DATABASE, DELETING, SELECTING BASED ON ATTRIBUTES AND PRINTING TO SCREEN:

# c.execute("INSERT INTO employees VALUES('Liam', 'Scott', 30000)") # adding values into table
# c.execute("DELETE from employees WHERE first='Emma'")
# conn.commit()

# CARE! Every time you run these commands they execute, i.e. keep adding the same values to table, so comment out once run once.

c.execute("SELECT * FROM employees WHERE last='Scott'") # choose items in database by last name

print(c.fetchall()) # print out all employees that satisfy the above requirement, last name Scott

emp_1 = Employee('John', 'Doe', 80000) # creating new variables using Employee function in employee.py
emp_2 = Employee('Jane', 'Doe', 80000)

# print(emp_1.first) # print to check it's worked
# print(emp_1.last)
# print(emp_1.pay)

# one way of inserting values into table using recently created variables:

# c.execute("INSERT INTO employees VALUES (?, ?, ?)", (emp_1.first, emp_1.last, emp_1.pay))
# conn.commit()

c.execute("SELECT * FROM employees WHERE last=?", ('Doe',)) # need comma inside brackets so computer reads as a tuple
# if there were multiple entries a comma would not be needed at the end it would just be like a normal tuple: (a, b, c)

print(c.fetchall())

# another way of inserting values into table using variables:

# c.execute("INSERT INTO employees VALUES (:first, :last, :pay)", {'first': emp_2.first, 'last': emp_2.last, 'pay': emp_2.pay})
# conn.commit()

c.execute("SELECT * FROM employees WHERE last=:last", {'last': 'Powell'})

print(c.fetchall())

# c.fetchone() # gets the next row in our result and returns that, if no rows available, returns none
# c.fetchmany(5) # returns 5 rows as a list, if no rows available, will return empty list
# c.fetchall() # returns remaining rows as a list, if none returns an empty list


conn.commit() # people often forget this part! commits changes

conn.close() # close connection - good practise

