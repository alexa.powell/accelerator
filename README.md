# Big heading
## Medium heading
### Small heading

*italics with one star around the word*
**bold with two stars**
***bold italics with three stars***

list:
* item 1
* item 2
	* item 2a

`inline code`

```
block code
```
> important point
