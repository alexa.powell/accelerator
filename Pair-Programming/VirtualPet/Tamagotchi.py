import time
from threading import *


class Tamagotchi(object):

    def __init__(self):
        self.hunger = 50
        self.fullness = 50
        self.tiredness = 50
        self.happiness = 50
        self.mood = 'happy'
        self.alive = True


    def CheckAlive(self):
        return self.alive

    def DisplayStats(self):
        if self.TammyHasDied():
            return
        else:
            print('\nHunger:', self.hunger, '\nFullness:', self.fullness, '\nTiredness:', self.tiredness, '\nHappiness:', self.happiness, '\nMood:', self.mood, "\n")

    def FeedTamagotchi(self):
        if self.TammyHasDied():
            return
        else:
            self.hunger -= 10
            self.fullness += 10

    def PlayWithTamagotchi(self):
        if self.TammyHasDied():
            return
        else:
            self.happiness += 10
            self.tiredness += 10

    def PutTamagotchiToBed(self):
        if self.TammyHasDied():
            return
        else:
            self.tiredness = 0

    def MakeTamagotchiPoop(self):
        if self.TammyHasDied():
            return
        else:
            self.fullness -= 10

    def TammyHasDied(self):
        if self.hunger >= 100:
            self.alive = False
            print("\nYour Tamagotchi has died!")
            return True
        else:
            return False

    def UpdateMood(self):
        if self.happiness >= 50:
            self.mood = 'happy'
        else:
            self.mood = 'sad'

        if self.hunger >= 70:
            self.mood = 'hangry' 

    def UpdateStats(self):
        if self.TammyHasDied():
            return
        else:
            self.hunger += 10
            self.happiness -= 10
            self.tiredness += 10

            self.UpdateMood()
           



def displayOptions():
    print("\nKeyboard Shortcuts:\n")
    print("(1) Feed Tammy")
    print("(2) Play with Tammy")
    print("(3) Put Tammy to bed")
    print("(4) Make Tammy poop")
    print("(5) Check on Tammy")

def play():

    print("Hi! My Name is Tammy. Please take care of me!")
    Tammy = Tamagotchi()
    Tammy.DisplayStats()

    def backgroundTimer():
        while(Tammy.CheckAlive()):
            Tammy.UpdateStats()
            time.sleep(15)    

    def takeUserInput():
        displayOptions()
        while(Tammy.CheckAlive()):
            y = input("What would you like to do? (Press '*' to display options) ")
            if y == "1": 
                Tammy.FeedTamagotchi()
            if y == "2":
                Tammy.PlayWithTamagotchi()
            if y == "3":
                Tammy.PutTamagotchiToBed()
            if y == "4":
                Tammy.MakeTamagotchiPoop()
            if y == "5":
                Tammy.UpdateMood()
                Tammy.DisplayStats()
            if y == "*":
                displayOptions()
    # create threads
    t = Timer(0.0, backgroundTimer)
    t2 = Timer(1.0, takeUserInput)

    # start threads
    t.start()
    t2.start()

if __name__ == '__main__':
    play()
    