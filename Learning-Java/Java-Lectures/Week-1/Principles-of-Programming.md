# Principles of Programming - How to learn programming languages

In Java you *must* use object-oriented programming - there is no other way to do it!

Suggested reading: Java - a beginner's guide.

## Before we begin...
* Do not box yourself into one language, you should, as a good software engineer, be able to pick up any new language with ease if you learn the right principles
* Poor language choice will create lots of incidental complexity

## What defines a programming language?
* Paradigm - classification of languages by their features
* Execution Model - how you go from human-readbale code to running on a machine
* Syntax - the textual structure of a valid programme. (Note, Java uses a compiler)
* Semantics - the meaning of the programme
* Type system - how the language prevents incorrect operations

## Paradigms:
* Imperative - how to compute
	* Procedural: instructions are grouped into blocks, or *'procedures'*
	* Object Oriented: objects have a state and code to modify their state
* Declarative
	* Functional: mathematical relationships between functions - this is the way we are used to, writing code that tells the computer what and how to do the exercises at hand.
	* Logical: facts and deductive rules are defined - like with prolog, you state something that is true, i.e. max(2, 3) = 3, so you are saying what it does rather than how to do it.

## Execution Models:
* Interpreted - parse and execute each line, e.g. Python
* Compilation ahead of time - convert source into native code once, then execute, e.g. Golang
* Just-in-time compilation - combination of the two, e.g. Java. This is sort of a combination of the two above options

## Syntax:
The syntax is the set of rules which define which sequence of characters forms a valid programme. Compilers will reject syntactically incorrect code.

## Semantics:
The set of rules that define the meaning of syntactic constructs. Semantic errors can cause compiler errors or bugs.

## Type Systems:
* Data types constrain the values that variables can take
* Type systems specify how data types are defined and how they can be used - trying to make type errors less likely
* The goal of the type system is to discourage type errors
* Type enforcement can occur at compile time, before run-time, and this is called static type-checking, or at run-time which is called dynamic type-checking.  

> "One of the most helpful concepts in the whole of programming is the notion of type, used to classify the kinds of object which are manipulated. A significant proportion of programming mistakes are detected by an implementation which does type-checking before it runs any programme. Types provide a taxonomy which helps people to think and to communicate about programmes" - R. Milner, 'Computing Tomorrow', (CUP, 1996), p264.

## Type Safety:
A type-safe language may have trapped errors (ones that can be handled gracefully) but can't have untrapped errors (ones that cause unpredictable crashes).  
Duck-Typing: If it looks like a duck and it quacks like a duck, it's a duck

In Java you always have what is called a 'Type tree' which always has a root, of type object. This is a class - you can create new types which extend (inherit from) a parent type. 

Java has (mostly) static type-checking whereas Python is dynamic, causing it to be slightly slower. With regards to the type paradigm, Java uses a labelled inheritance tree whereas Python would simply have a type - duck. Both have type-safety.
