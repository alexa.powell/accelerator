/* This is the MySQL code to create and fill data into a table representing the Ship Tracking
ER diagram I did in week 2. Notes for me: The ALTERs at the end are there because they are 
referencing a parent in a table which hasn't been created yet. Each time I have an empty line
that will enter the commands above and there should be a line after it confirming it has worked
in terminal. You can't fill in any info into a FK column unless the table being referenced has
been created and the info is findable/referencable in the parent column (in the parent table).
The way around this is to fill in all the info normally in the INSERT INTO statement but add the
FKs at the end for connections where the child table (with the FK) is created before the parent
table (with the PK). For example, in Ship, I can put the FK S_Type in the CREATE statement since
it references Ship_Type which has already be created, and when I fill Ship in, the entries in
S_Type already exist in the parent column: Type_of_ship in Ship_Type. I also fill in Port_Name and 
Port_visit_start_date when I create Ship, however they are not FKs at this point so this is allowed.
Once the tables Port and Port_Visit are created and filled in, I can then create the connection
between the parents and children by creating FKs in Ship, so I do this via an ALTER at the end. */

DROP DATABASE IF EXISTS Ship_Tracking;
CREATE DATABASE IF NOT EXISTS Ship_Tracking;
USE Ship_Tracking;

CREATE TABLE Ship_Type (
    Type_of_ship VARCHAR(20) NOT NULL,
    Hull VARCHAR(20) NOT NULL,
    Tunnage INT NOT NULL,
    PRIMARY KEY (Type_of_ship)
);

INSERT INTO `Ship_Type` (`Type_of_ship`, `Hull`, `Tunnage`) VALUES
('small sailboat', 'v-bottom', '500'),
('medium ship', 'hard-chined', '1000'),
('big ship', 'chined', '2000'),
('bigger ship', 'soft-chined', '3000');

CREATE TABLE Ship (
    S_Name VARCHAR(20) NOT NULL,
    Owner_of_ship VARCHAR(20) NOT NULL,
    S_Type VARCHAR(20),
    Port_Name VARCHAR(20) NOT NULL,
    Port_visit_start_date DATE NOT NULL,
    PRIMARY KEY (S_Name),
    FOREIGN KEY (S_Type) REFERENCES Ship_Type (Type_of_ship) ON DELETE CASCADE
);

INSERT INTO `Ship` (`S_name`, `Owner_of_ship`, `S_Type`, `Port_Name`, `Port_visit_start_date`) VALUES
('The Victoria', 'Victoria', 'big ship', 'Liverpool port', '2018-12-19'),
('The Edmund', 'Edmund', 'bigger ship', 'Manchester port', '2018-12-18'),
('The Queen Mary', 'Queenie', 'small sailboat', 'London port', '2018-12-17'),
('The Debbie', 'Frank', 'medium ship', 'Barcelona port', '2018-12-19');

CREATE TABLE Country (
    C_Name VARCHAR(20) NOT NULL,
    Continent VARCHAR(20) NOT NULL,
    PRIMARY KEY (C_Name) 
);

INSERT INTO `Country` (`C_Name`, `Continent`) VALUES
('UK', 'Europe'),
('Spain', 'Europe'),
('France', 'Europe'),
('Tunisia', 'Africa');

CREATE TABLE Ocean (
    O_Name VARCHAR(20) NOT NULL,
    PRIMARY KEY (O_Name)
);

INSERT INTO `Ocean` (`O_Name`) VALUES
('Pacific'),
('Indian'),
('Atlantic');

CREATE TABLE Port (
    P_Name VARCHAR(20) NOT NULL,
    Country_Name VARCHAR(20) NOT NULL,
    Port_visit_start_date DATE NOT NULL,
    Ship_Name VARCHAR(20) NOT NULL,
    Ocean_Name VARCHAR(20) NOT NULL,
    PRIMARY KEY (P_Name),
    FOREIGN KEY (Country_Name) REFERENCES Country (C_Name) ON DELETE CASCADE,
    FOREIGN KEY (Ship_Name) REFERENCES Ship (S_Name) ON DELETE CASCADE,
    FOREIGN KEY (Ocean_Name) REFERENCES Ocean (O_Name) ON DELETE CASCADE
);

INSERT INTO `Port` (`P_Name`, `Country_Name`, `Port_visit_start_date`, `Ship_Name`, `Ocean_Name`) VALUES
('Liverpool port', 'UK', '2018-12-19', 'The Victoria', 'Atlantic'),
('Manchester port', 'UK', '2018-12-18', 'The Edmund', 'Atlantic'),
('London port', 'UK', '2018-12-17', 'The Queen Mary', 'Atlantic'),
('Barcelona port', 'Spain', '2018-12-19', 'The Debbie', 'Atlantic');

CREATE TABLE Port_Visit (
    StartDate DATE NOT NULL,
    EndDate DATE NOT NULL,
    Ship_Name VARCHAR(20) NOT NULL,
    PRIMARY KEY (StartDate, Ship_Name),
    FOREIGN KEY (Ship_Name) REFERENCES Ship (S_Name) ON DELETE CASCADE
);

INSERT INTO `Port_Visit` (`StartDate`, `EndDate`, `Ship_Name`) VALUES
('2018-12-19', '2018-12-20', 'The Victoria'),
('2018-12-18', '2018-12-20', 'The Edmund'),
('2018-12-17', '2018-12-19', 'The Queen Mary'),
('2018-12-19', '2018-12-21', 'The Debbie');

CREATE TABLE Ship_Movement (
    SM_Date DATE NOT NULL,
    SM_Time TIME NOT NULL,
    Ship_Name VARCHAR(20) NOT NULL,
    Latitude VARCHAR(30) NOT NULL,
    Longitude VARCHAR(30) NOT NULL,
    PRIMARY KEY (SM_Date, SM_Time, Ship_Name),
    FOREIGN KEY (Ship_Name) REFERENCES Ship (S_Name) ON DELETE CASCADE
);

INSERT INTO `Ship_Movement` (`SM_Date`, `SM_Time`, `Ship_Name`, `Latitude`, `Longitude`) VALUES
('2018-12-01', '17:00:00', 'The Victoria', '12.34567', '23.45678'),
('2018-11-30', '18:00:00', 'The Edmund', '45.34567', '63.45678'),
('2018-12-11', '19:00:00', 'The Queen Mary', '21.34567', '10.45678'),
('2018-12-15', '20:00:00', 'The Debbie', '12.35567', '29.45678');

CREATE TABLE Ship_at_port(
    Port_Name VARCHAR(20) NOT NULL,
    Ship_Name VARCHAR(20) NOT NULL,
    PRIMARY KEY (Port_Name, Ship_Name)
);

INSERT INTO `Ship_at_port` (`Port_name`, `Ship_Name`) VALUES
('Liverpool port', 'The Victoria'),
('Manchester port', 'The Edmund'),
('London port', 'The Queen Mary'),
('Barcelona port', 'The Debbie');

ALTER TABLE Ship
ADD FOREIGN KEY (Port_Name) REFERENCES Port (P_Name) ON DELETE CASCADE,
ADD FOREIGN KEY (Port_visit_start_date) REFERENCES Port_Visit (StartDate) ON DELETE CASCADE;

ALTER TABLE Port
ADD FOREIGN KEY (Port_visit_start_date) REFERENCES Port_Visit (StartDate) ON DELETE CASCADE;