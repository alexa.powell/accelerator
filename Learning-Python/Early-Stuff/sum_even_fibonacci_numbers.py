#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 28 18:04:17 2018

@author: powella
"""

"""Early attempt at writing a function to find the sum
of all even numbers in the fibonacci sequence"""

a, b = 1, 2
even = [2]
while (a + b < 40):
    c = a + b
    if c % 2 == 0:
        even.append(c)
    a = b
    b = c
total = sum(even)
print(total)