import unittest
from game_of_life_refactor_alexa import GameOfLife

class test_game_of_life(unittest.TestCase):
    def setUp(self):
        print('setting up')
        
    def tearDown(setUp):
        print()
        
    def test_GameOfLife(self):
        testgame = GameOfLife(2,[(0,0)])
        
        print('\nTesting whether the grid initialised correctly')
        for cell in [(0,0)]:
            self.assertTrue(testgame.grid_status[cell])
        for cell in [(0,1),(1,0),(1,1)]:
            self.assertFalse(testgame.grid_status[cell])
        
        print('\nTesting findNeighbours')
        for neighbours in testgame.findNeighbours((0,0)):
            self.assertTrue(neighbours in [(0,1),(1,1),(1,0)])
        self.assertEqual(len(testgame.findNeighbours((0,0))),3)
        
        print('\nTesting nextGenerationStatus')
        for cell in [(0,0),(0,1),(1,0),(1,1)]:
            self.assertFalse(testgame.nextGenerationStatus(cell))
        
        print('\nTesing updateGrid')
        testgame.updateGrid()
        for cell in [(0,0),(0,1),(1,0),(1,1)]:
            self.assertFalse(testgame.grid_status[cell])

if __name__ == '__main__':
    unittest.main()
