import java.io.BufferedReader;
import java.io.IOException;
import java.io.*;

public class Main {


    public static void main(String[] args)throws IOException {
        Calculator calc = new Calculator();
        // testobj.add(1, 2);
        System.out.println("Enter what you would like to calculate, for example, 3+2 or 6/2");
        BufferedReader myReader= new BufferedReader(new InputStreamReader(System.in));
        String request= myReader.readLine();

        String firstNumberString = request.substring(0, 1);
        String secondNumberString = request.substring(2, 3);
        String operator = request.substring(1, 2);
        int firstNumber = Integer.parseInt(firstNumberString);
        int secondNumber = Integer.parseInt(secondNumberString);

        if (operator.equals("+")) {
            calc.add(firstNumber, secondNumber);
        }

        else if (operator.equals("-")) {
            calc.minus(firstNumber, secondNumber);
        }

        else if (operator.equals("-")) {
            calc.multiply(firstNumber, secondNumber);
        }

        else if (operator.equals("-")) {
            calc.divide(firstNumber, secondNumber);
        }

        else {
            System.out.println("operator not recognised");
        }
    }
}