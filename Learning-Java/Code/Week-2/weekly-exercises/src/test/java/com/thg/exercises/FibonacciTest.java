package com.thg.exercises;

import static org.junit.Assert.assertTrue;
import java.util.Arrays;
import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class FibonacciTest {
    private static fibonacci fib = new fibonacci(5);
    int[] sequence = fib.getSeq();

    @Test
    public void checkFibArrayEqual() {
        assertArrayEquals("Should return correct array", new int[]{1, 2, 3, 5, 8}, sequence);
    }

    @Test
    public void checkFibArrayNotEqual() {
        assertFalse("Should return a different array to the incorrect one provided", Arrays.equals(new int[]{1, 2, 3, 5, 9}, sequence));
    }
}


