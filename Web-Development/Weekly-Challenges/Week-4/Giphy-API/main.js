let img = document.querySelector('img');
let refresh = document.querySelector('#refresh');
let search = document.querySelector('#search');
let searchValue = 'cats';

let refreshGifSearch = () => {
    fetch('https://api.giphy.com/v1/gifs/translate?api_key=hhkig6EJA7Kgf1KT1GywFIzpNzFwuYRg&s=' + searchValue, {mode: 'cors'})
    .then((response) => response.json())
    .then((response) => {
        img.src=response.data.images.original.url;
        console.log("yo");
    })
    .catch(() => {
        img.src = 'https://imgix.bustle.com/2017/4/27/c5576501-1a7a-43b8-b5e4-9f0aed1083e0.jpg?w=970&h=546&fit=crop&crop=faces&auto=format&q=70';
    })
};

search.addEventListener('submit', function(e) {
    e.preventDefault();
    searchValue = document.querySelector('input').value;
    refreshGifSearch();
})

refresh.addEventListener('click', function(e) {
    e.preventDefault();
    refreshGifSearch();
});

