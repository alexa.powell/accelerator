# Exercise 2 - DML/SQL Queries

* Count the number of distinct salary values in the database:  
~~~~sql
SELECT COUNT(DISTINCT(salary))  
FROM employee
~~~~
* Find the names and average salaries of all departments whose average salary is greater than 42000:
~~~~sql
SELECT dname, e.avg_salary  
FROM (SELECT dno, AVG(salary) avg_salary  
      FROM employee  
      GROUP BY dno) e, department  
WHERE e.dno = dnum AND e.avg_salary > 42000;
~~~~
* For each department, retrieve the department number, the number of employees in the department, and their average salary:
~~~~sql
SELECT dno, AVG(salary) avg_salary, COUNT(DISTINCT(NI)) count_employees  
FROM employee  
GROUP BY dno;
~~~~
* For each project, retrieve the project number, the project name, and the number of employees who work on that project:
~~~~sql
SELECT pnum, pname, e.num_of_employees  
FROM (SELECT pno, COUNT(DISTINCT(eNI)) num_of_employees  
      FROM works_on  
      GROUP BY pno) e, project  
WHERE e.pno = pnum;  
~~~~
* For each project on which more than two employees work, retrieve the project number, the project name, and the number of employees who work on the project:
~~~~sql
SELECT pnum, pname, e.num_of_employees  
FROM (SELECT pno, COUNT(DISTINCT(eNI)) num_of_employees  
      FROM works_on  
      GROUP BY pno) e, project  
WHERE e.pno = pnum AND e.num_of_employees > 2;  
~~~~
* Find the sum of the salaries of all employees of the ‘Research’ department, as well as the maximum salary, the minimum salary, and the average salary in this department:
~~~~sql
SELECT SUM(e.salary) sum, MAX(e.salary) max, MIN(e.salary) min, AVG(e.salary) avg
FROM (SELECT salary
      FROM employee, department
      WHERE dno = dnum AND dname = 'research') e;
~~~~
* Retrieve the total number of employees in the company and the number of employees in the ‘Research’ department:
~~~~sql
SELECT COUNT(DISTINCT(NI)) total, e.research_total
FROM (SELECT COUNT(DISTINCT(NI)) research_total
      FROM employee, department
      WHERE dno = dnum AND dname = 'research') e, employee
GROUP BY e.research_total;
~~~~
* Retrieve the national insurance numbers of all employees who work on project number 1, 2, or 3:
~~~~sql
SELECT NI
FROM employee e, works_on w
WHERE e.NI = w.eNI AND (w.pno = 1 OR w.pno = 2 OR w.pno = 3);
~~~~
* For each department having more than five employees, retrieve the department number and the number of employees making more than £40,000:
~~~~sql
SELECT e.salary, e.dno
FROM (SELECT dno, a.count
      FROM (SELECT dno, COUNT(DISTINCT(NI)) count
            FROM employee
            GROUP BY dno) a
      WHERE a.count > 2) b, employee e
WHERE e.salary > 40000 AND e.dno = b.dno;
~~~~