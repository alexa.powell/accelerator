#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 28 16:08:38 2018

@author: powella
"""

"""Learning stuff in Python early on in the process, messing around with functions"""

import datetime


def Largest_Odd(): # try using a function to call at the end of all the code so can have multiple functions in one working file
    odd = []
    numbers = []
    numbers.append(int(input("Enter three integers: "))) # using the numbers[0] = approach doesn't work as it searches for the 0th entry to replace, so use append instead
    numbers.append(int(input(""))) # input only takes one bit of info each time, can use .split to separate the input based on commas
    numbers.append(int(input("")))
    print(" ") # this just adds an extra line before the output at the end, makes it look nicer
    i=0
    while (i<3):
        if numbers[i]%2 != 0: # i.e. if odd, add to list called odd
            odd.append(numbers[i])
        i=i+1
    if len(odd) == 0: # length of list to check if there actually are any odd numbers
        print('There are no odd numbers')
    else:
        odd.sort(reverse=True) # reverse=True changes sort to descending order, sending largest to 0th position
        print('The largest odd number is', odd[0])


def PrintX():
    numXs = int(input('How many times should I print the letter X? '))
    toPrint = ''
    i = 0
    while (i < numXs):
        toPrint = toPrint + "X "
        i = i + 1
    print(toPrint)


def RootPower():
    test = 0
    i = int(input("Enter an integer:"))
    for root in range(1, i):
        for pow in range(1, 6):
            if root**pow == i:
                test = 1
                print("Your integer", i, "is equal to", root, "to the power of", pow)
    if test == 0:
         print("There are no distinct root-power pairs which equal the integer you have entered")


def AddString():
    total = 0
    s = '1.23,2.4,3.123'
    print("The sum of the numbers in the string", s, "is:")
    s = s.split(",")
    for i in range(len(s)):
        total = total + float(s[i])
    print(total)


def Strings_in_Strings():
    a = str(input("Enter a word: "))
    b = str(input("Enter another word: "))
    if a in b:
        print(a, "is inside", b)
    elif b in a:
        print(b, "is inside", a)
    else:
        print("Neither", a, "nor", b, "is inside the other")


def Find_Cube():
    x = int(input("Enter an integer: "))
    ans = 0
    while ans**3 < abs(x):
        ans = ans + 1
    if ans**3 != abs(x):
        print (x, "is not a perfect cube")
    else:
        if x < 0:
            ans = -ans
        print("Cube root of", x, "is", ans)


def Date(day, month, year):
    d = int(day)
    m = int(month)
    y = int(year)
    print(datetime.date(y, m, d))


def main(): # here you write the functions you want to run
    # Largest_Odd()
    # PrintX()
    # RootPower()
    # Strings_in_Strings()
    # Find_Cube()
    Date(4, 9, 1995)

main() # run main so the chosen functions called within it run in the console
