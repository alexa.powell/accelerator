# SCSS Workshop

Design your own "corny" website to advertise THG tech.

## Rules

Don't change any HTML or image files. Your work should be in the "scss" / "main.scss" file. 

## Compile SCSS
Install node-sass (globally)
```
sudo npm i -g node-sass
```
Compile styles
```
./compile-styles.sh
```