import React, { Component } from 'react';

class Counter extends Component {
    constructor() {
        super();

        this.state = {
            count: 5
        }
    }

    increaseCount = () => {
        // const newState = {
        //     count: this.state.count + 1
        // }
        // this.setState(newState);

        // or

        this.setState((prevState) => ({count: prevState.count + 1}));
    }

    render() {
        return (
            <section>
                <span>{this.state.count} </span>
                <button onClick = {this.increaseCount} >Add 1</button>
            </section>
        )
    }
}

export default Counter;