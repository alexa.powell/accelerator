
describe('NumberList', () => {
    let listUnderTest = null;
    
    beforeEach(() => {
        listUnderTest = new NumberList();
    });

    describe('after initialisation', () => {
        it('should sum to zero', () => {
            expect(listUnderTest.sumAll()).to.equal(0);
        });

        it('should produce a product equal to null', ()=> {
            expect(listUnderTest.multiplyAll()).to.be.null;
        });
    });
});