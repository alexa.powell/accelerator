from flask import Flask, render_template, request
import addpost

def writeTXT(name, post):
    with open('posts.txt', 'a') as p:    #so don't have to close
        p.write(post + "---")
    nameTXT = open('names.txt', 'a')
    nameTXT.write(name + "---")
    nameTXT.close()

def createProfile(username, password):
    with open('username.txt', 'a') as p:    #so don't have to close
        p.write(str(username) + "---")
    nameTXT = open('password.txt', 'a')
    nameTXT.write(str(password) + "---")
    nameTXT.close()

def login(username, password):
    blog_dict = {}

    names_open = open("username.txt", "r")
    names_read = names_open.read()
    names = list(names_read.strip().split("---"))
    del names[-1]

    posts_open = open("password.txt", "r")
    posts_read = posts_open.read()
    posts = list(posts_read.strip().split("---"))
    del posts[-1]

    for i in range(len(names)):
        blog_dict[names[i]] = posts[i]

    if blog_dict[username] == password:
        return True
    else:
        return False

def numMembers():
    members_open = open("members.txt", "r")
    members_read = members_open.read()
    members_open.close()
    return members_read

def addMembers():
    members_open = open("members.txt", "r")
    members_read = int(members_open.read())
    members_open.close()
    members_read += 1
    members_read = str(members_read)
    members_open = open("members.txt", "w")
    members_open.write(members_read)
    members_open.close()


app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/home')
def home():
    return render_template('index.html')

@app.route('/pages')
def projectspiet():
    return render_template('pages.html')

@app.route('/blog-blank')
def blog_blank():
    return render_template('blog-blank.html')

@app.route('/blog-creating-a-blog')
def blog_creating_a_blog():
    return render_template('blog-creating-a-blog.html')

@app.route('/blog-creating-a-database')
def blog_creating_a_database():
    return render_template('blog-creating-a-database.html')

@app.route('/projects-piet-mondrian')
def projects_piet_mondrian():
    return render_template('projects-piet-mondrian.html')

@app.route('/projects-search-engine')
def projects_search_engine():
    return render_template('projects-search-engine.html')

@app.route('/projects-ascii-spinners')
def projects_ascii_spinners():
    return render_template('projects-ascii-spinners.html')

@app.route('/analytics')
def analytics():
    return render_template('analytics.html')

@app.route('/add-a-blog-post', methods = ['GET', "POST"])
def add_a_blog_post():
    if request.method == "POST":
        name = request.form["Name"]
        post = request.form["Post"]
        username = request.form["Username"]
        password = request.form["Password"]
        if login(username, password) is True:
            writeTXT(name, post)
        return render_template('add-a-blog-post.html')
    return render_template('add-a-blog-post.html', Likes=7)

@app.route('/search', methods = ['GET', "POST"])
def search():
    if request.method == "POST":
        name = request.form["Search"]
        post = addpost.add_new_posts(str(name))
        return render_template("search.html", Name = name, Post = post)
    return render_template('search.html')

@app.route('/create-blog-profile', methods = ['GET',"POST"])
def create_a_profile():
    if request.method == "POST":
        username = request.form["Username"]
        password = request.form["Password"]
        createProfile(username, password)
        addMembers()
        return render_template('create-a-profile.html', Members= numMembers(), Username = username)
    return render_template('create-a-profile.html', Members= numMembers())


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')


""" 
def write_commentTXT(name, post):
    postTXT = open('comments.txt', 'a')
    postTXT.write(post + "---")
    postTXT.close()
    nameTXT = open('name_comments.txt', 'a')
    nameTXT.write(name + "---")
    nameTXT.close()
"""