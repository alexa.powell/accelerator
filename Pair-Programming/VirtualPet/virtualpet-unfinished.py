class Tamagotchi(object):


    def __init__(self):
        self.hunger = 50
        self.tiredness = 50
        self.happiness = 50
        self.alive = True


    def DisplayStats(self):
        self.fullness = 100 - self.hunger
        print('Hunger:', self.hunger, '\nFullness:', self.fullness, '\nTiredness:', self.tiredness, '\nHappiness:', self.happiness)


    def FeedTamagotchi(self):
        self.hunger -= 10
        return self.hunger


    def PlayWithTamagotchi(self):
        self.tiredness += 10
        self.happiness += 10
        return self.tiredness, self.happiness


    def PutTamgotchiToBed(self):
        self.tiredness = 0
        return self.tiredness


    def MakeTamagotchiPoop(self):
        self.hunger += 10
        return self.hunger


    def ChangesOverTime(self):
        self.hunger += 5
        self.tiredness += 5
        self.happiness -= 5
        return self.hunger, self.tiredness, self.happiness


    def Hangry(self):
        if self.hunger >= 70:
            self.happiness = 20
            print('I\'m hangry!!!')
        return self.happiness


    def Dead(self):
        if self.hunger >= 100:
            self.alive = False
            print('Your Tamagotchi has died of hunger!!!')