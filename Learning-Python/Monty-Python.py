import random


def AllocateDoor():
    doors = ['goat', 'goat', 'goat']
    ferrari = random.randrange(0, 3)
    doors[ferrari] = 'ferrari'
    return doors


def ChooseFirstDoor():
    chosen_door = random.randrange(0, 3)
    return chosen_door


def OpenGoatDoor(chosen_door, doors):
    if doors[chosen_door] == 'ferrari':
        # Chosen a ferrari so making a random choice between two remaining doors to open
        available_doors = []
        for door in range(3):
            if door != chosen_door:
                available_doors.append(door)
        opened_door = random.choice(available_doors)
    else:
        # Chosen a goat so making sure the other goat is shown
        ferrari = doors.index('ferrari')
        for door in range(3):
            if door != chosen_door and door != ferrari:
                opened_door = door
    return opened_door


def SwitchDoors(chosen_door, opened_door):
    for door in range(3):
        if door != chosen_door and door != opened_door:
            return door


def PlayGame(iterations, action):

    print("\nInitialising simulations...")
    result = {"goat": 0, "ferrari": 0}

    for iteration in range(iterations):
        doors = AllocateDoor()
        chosen_door = ChooseFirstDoor()
        opened_door = OpenGoatDoor(chosen_door, doors)

        if action == 'stick':
            if doors[chosen_door] == 'goat':
                result["goat"] += 1
            else:
                result["ferrari"] += 1

        elif action == 'switch':
            new_chosen_door = SwitchDoors(chosen_door, opened_door)
            if doors[new_chosen_door] == 'goat':
                result["goat"] += 1
            else:
                result["ferrari"] += 1

    return result


if __name__ == "__main__":

    print("\nWelcome to the Monty Hall problem simulation!")
    iterations = int(input("\nHow many simulations would you like to run? "))
    action = str(input("""\nWould you like to stick with your original choice or switch to the new door in the simulations?
                      \nPlease type stick or switch: """))

    result = PlayGame(iterations, action)
    print("\nThe simulations got a final result of winning the ferrari", result["ferrari"], "times and getting the goat", result["goat"], "times!")
    print("So you won a ferrari {0}% of the time with your chosen strategy".format((result["ferrari"]/iterations)*100))

    if result["ferrari"] >  result["goat"]:
        print("\nCongratulations! Your strategy was successful!\n")
    elif result["goat"] > result["ferrari"]:
        print("\nCommiserations! Your strategy wasn't successful!\n")
    else:
        print("\nWow! Your strategy was successful exactly half the time! Why not try with more iterations to get a more difinitive result?\n")