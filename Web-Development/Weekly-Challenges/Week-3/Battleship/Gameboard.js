const Ship = require('./Battleship');

const Gameboard = (size) => {
    let board = new Array(size);
    for (i = 0; i < size; i++) {
        board[i] = new Array(size);
        for (j = 0; j < size; j++) {
            board[i][j] = {x: i, y: j, ship: false, hit: false};
        }
    }
    
    let shipSunkStatuses = {};
    const populateShipList = (shipName) => {
        shipSunkStatuses[shipName] = false;
    };
    
    let shipPositionList = [];
    const populatePositionList = (shipName, x, y, length, orientation) => {
        for (i = 0; i < length; i++) {
            if (orientation === 'h') {
                shipPositionList.push({ship: shipName, x, y: y + i, position: i});
                updateBoardWithShip(x, y + i);
            } else {
                shipPositionList.push({ship: shipName, x: x + i, y, position: i});
                updateBoardWithShip(x + i, y);
            }
        }
    };

    const updateBoardWithShip = (x, y) => {
        board[x][y]['ship'] = true;
    };

    const placeShip = (shipName, x, y, length, orientation) => {
        if (orientation !== 'v' && orientation !== 'h') {
            throw new Error("orientation not valid, input 'h' or 'v'");
        } else if (x < 0 || x >= size || y < 0 || y >= size) {
            throw new Error("coordinates not on board");
        }
        shipName = Ship(length);
        // fill in shipList
        populateShipList(shipName);
        // fill in shipPositionList
        populatePositionList(shipName, x, y, length, orientation);
        return shipName;
    };

    const didAttackHitShip = (x, y) => {
        if (board[x][y].ship === true) {
            return true;
        } else {
            return false;
        }
    };

    const implementAttack = (x, y) => {
        if (didAttackHitShip(x, y)) {
            for (let position of shipPositionList) {
                if (position.x === x && position.y === y) {                    
                    position.ship.hit(position.position);
                }
            }
        }
        //board[i][j].hit = true;
    };

    return { board, placeShip, shipSunkStatuses, populateShipList, shipPositionList, populatePositionList, updateBoardWithShip, didAttackHitShip, implementAttack, };
};

module.exports = Gameboard;