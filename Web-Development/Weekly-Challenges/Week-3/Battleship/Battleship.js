const Ship = (length) => {
    let hits = new Array(length).fill(false);
    const hit = (position) => {
        if (position >= 0 && position < length) {
            hits[position] = true;
        }
    };
    const isSunk = () => {
        var sunk = true;
        for (i = 0; i < length; i++) {
            if (hits[i] === false) {
                sunk = false;
            }
        }
        return sunk;
    };
    return { length, hits, hit, isSunk, };
};

module.exports = Ship;