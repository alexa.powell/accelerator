
// no parameters:
const message = () => 'hello';
console.log('called without brackets:', message);
console.log('called with brackets:', message());

// one parameter:
const double = x => 2 * x; 
console.log(double(3));

const doubleLonger = x => {return 2 * x};
console.log(doubleLonger(4));

// object creation, setting literals:
const me = (name, hobby) => ({name: name, hobby: hobby});
console.log(me('alexa', 'rollerskating'));
