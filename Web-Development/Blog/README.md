#Blog Challenge

In here is my first attempt at any kind of web development, from making a flask app to using html and css. I worked on this project with Simon and although the code itself is a little messy in places, with styling of various elements being done through an adhoc combination of inline and external css due partly to a lack of understanding about how to effectively implement the changes we wanted and partly to time pressures, however I have not refactored any of it and have left it as it is as a nice time capsule of a first attempt at anything of that kind.


To run this, simply run webapp.py in your terminal by entering `python webapp.py` and going to the local host address returned in your terminal, usually `http://0.0.0.0:5000/` since it is simply running on localhost. Not all pages have meaningful content but you can create anaccount and make blog posts if you are a member, as well as searching for blog posts that have been submitted by others. Search 'Romeo, Romeo...' or 'Romeo and Juliet' in the `Find a Blog Post` tab to see some example blog posts submitted through the `Add a Blog Post` page. Enjoy!
