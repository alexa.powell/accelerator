# Exercises 3:

1) a) Retrieve the names of all employees in department 5 who work more than 10 hours per week on the “Product Mega” project:
~~~~sql
SELECT fname, lname  
FROM employee e, works_on w, project p  
WHERE e.ni = w.eni AND e.dno = 5 AND w.pno = p.pnum AND p.pname = 'Product Mega' AND w.hours > 10;
~~~~
The e, w and p here aren't necessary but I have included them just for myself to keep track of which tables I'm looking at.

1) b) Show the resulting salaries if every employee working on the ‘Product Alfa’ project is given a 10% raise:
~~~~sql
SELECT salary*1.1  
FROM employee, works_on, project  
WHERE pname = 'Product Alfa' AND ni = eni AND pno = pnum;
~~~~
1) c) List the names of all employees who have a dependent with the same first name as themselves:
~~~~sql
SELECT fname, lname  
FROM employee, dependent  
WHERE fname = dependent_name AND eni = ni;
~~~~
1) d) Retrieve a list of employees and the projects they are working on, ordered by department and, within each department, alphabetically by last name, first name:
~~~~sql
SELECT fname, lname, pname, dname  
FROM employee, works_on, project, department  
WHERE eni = ni AND pno = pname AND dno = dnum  
ORDER BY dname, lname, fname;
~~~~
1) e) Find the names of all employees who are directly supervised by ‘Franklin Wong’:
~~~~sql
SELECT fname, lname  
FROM employee e, employee s  
WHERE e.superni = s.ni AND s.fname = 'Franklin' AND s.lname = 'Wong';
~~~~
> In answers, it says e.lname and e.fname in the WHERE clause but it should be d instead of e (in mine, s).

2) a) Retrieve the names of all senior students majoring in ‘cs’ (computer science):
~~~~sql
SELECT fname, lname  
FROM student  
WHERE major = 'cs' AND class = 4;
~~~~
2) b) Retrieve the names of all courses taught by Professor King in 2007 and 2008:
~~~~sql
SELECT c.course_name  
FROM course c, section s  
WHERE c.course_number = s.course_number AND s.instructor = 'King' AND (s.year = 07 or s.year = 08);
~~~~
2) c) For each section taught by Professor King, retrieve the course number, semester, year and number of students who took the section:
~~~~sql
SELECT sec.course_number, sec.semester, sec.year, COUNT(st.student_number)  
FROM section sec, student st, course c  
WHERE sec.instructor = 'King' AND sec.course_number = c.course_number AND c.department = student.major;
~~~~
Just spotted the Grade_Report table in the database so will re-do with this information but will leave the above answer as I think it too works.
~~~~sql
SELECT s.course_number, s.semester, s.year, COUNT(DISTINCT(g.student_number))  
FROM section s, grade_report g  
WHERE s.instructor = 'King' AND s.section_identifier = g.section_identifier  
GROUP BY s.course_number, s.semester, s.year;
~~~~
2) d) Retrieve the name and transcript of each senior student (Class = 4) majoring in CS. A transcript includes course name, course number, credit hours, semester, year, and grade for each course completed by the student:
~~~~sql
SELECT st.name, c.course_name, c.course_number, c.credit_hours, sec.semester, sec.year, g.grade  
FROM student st, course c, section sec, grade_report g  
WHERE st.class = 4 AND st.major = 'cs' AND c.course_number = s.course_number AND sec.section_identifier = g.section_identifier AND g.student_number = st.student_number;
~~~~
2) e) For all instructors who have taught some course, find their names and the course ID and name of the courses they taught:
~~~~sql
SELECT s.instructor, c.course_number, c.course_name  
FROM section s, course c  
WHERE c.course_number = s.course_number;
~~~~
> In answers, the tables teaches is referenced and there is no such table
