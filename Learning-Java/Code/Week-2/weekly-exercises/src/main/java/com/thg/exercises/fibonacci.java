package com.thg.exercises;
import java.util.Arrays;

public class fibonacci {

    private int numValues;

    public fibonacci(int n) {
        this.numValues = n;
    }

    private int[] Calculate() {
        int prev1 = 1;
        int prev2 = 2;

        int fibArray[] = new int [numValues];

        int i = 0;
        while (i<numValues) {
            int next = prev1 + prev2;
            fibArray[i] = prev1;
            prev1 = prev2;
            prev2 = next;
            i ++;
        }
        return fibArray;
    }

    public int[] getSeq() {
        int[] sequence = Calculate();
//        System.out.println(sequence);
        return sequence;
    }
}
