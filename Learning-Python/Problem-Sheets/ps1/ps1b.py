#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 17 12:10:37 2018

@author: powella
"""

# input all the info provided in the question
portion_down_payment = 0.25
current_savings = 0.0
r = 0.04

# get user input
total_cost = int(input('Enter the cost of your dream house: '))
portion_saved = float(input('Enter what percentage of your salary you would like to save (as a decimal): '))
annual_salary = int(input('Enter your annual salary: '))
semi_annual_raise = float(input('Enter your semi-annual salary raise: (as a decimal)'))

# calculate necessary values
money_needed = total_cost*portion_down_payment

i = 0
while current_savings < money_needed :
    
    # for this scenario, use an if loop to increase salary and 
    # calculate monthly salary and amount to be put away inside while loop
    
    if i%6 == 0 and i != 0: # adding this in to execute every 6 months but not on the first loop
        annual_salary = annual_salary*(1+semi_annual_raise)
    
    monthly_salary = annual_salary/12
    putting_away = monthly_salary*portion_saved
    
    current_savings += current_savings*r/12 + putting_away
    i += 1

#print('it will take you', i, 'months to save up for your dream house')

years = int(i/12)
months = i%12

if years != 0 and months != 0:
    print('\nIt will take you', years, 'years and', months, 'months to save up for your dream house')
elif years == 0:
    print('\nIt will take you', months, 'months to save up for your dream house')
else:
    print('\nIt will take you', years, 'years to save up for your dream house')
        
