#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 15 09:39:04 2018

@author: powella
"""

""" This is a first attempt at creating a database out of a dictionary """

movies = open("movies.txt", "r")
m = movies.read()

mdatabase = {"name": [], "genres": [], "description": []} # sets up dictionary with 3 keys

lines = list(m.split('\n')) # creates a list where each entry corresponds to each line/entry in the movie file

for i in range(400):
    columns = list(lines[i].split('\t')) # creates a list whose entries are the components of each line seaparated by a tab
    mdatabase["name"].append(columns[1]) # allocates the ith entry in the names list of our database to be equal to the second item in our tab separated line (since the first item is just a number)
    genre_options = list(columns[2].split(",")) # creates a list out the entries in the third column of our line, separating the entries using commas as delimiters
    mdatabase["genres"].append(genre_options) # allocates the list 'genre_options' as the ith entry in the genres list
    mdatabase["description"].append(columns[3]) # allocates the final entry of columns - the description, to the description key in our dictionary

movies.close()

def name_search():
    name = input('Enter the name of the movie you wish to learn about: ')
    for i in range(400):
        if name in mdatabase["name"][i]:
            info = mdatabase["description"][i]
            print(mdatabase["name"][i], ':', info)
    if name not in mdatabase["name"]:
        print('It seems the movie you are looking for isn\'t here, please check the spelling and try again')
    
name_search()

def genre_list():
    all_genres = []
    for i in range(400):
        x = 0
        while x < len(mdatabase["genres"][i]):
            if mdatabase["genres"][i][x] not in all_genres:
                all_genres.append(mdatabase["genres"][i][x])
            x += 1
    all_genres.sort()
    print(all_genres)

#genre_list()

def genre_search():
    genre = input('Enter a genre you are interested in out of: \nAction\nAdventure\nAnimation\nBiography\nComedy\nCrime\nDrama\nFamily\nFantasy\nHistory\nHorror\nMusic\Musical\nMystery\nRomance\nSci-Fi\nSport\nThriller\nWar\nWestern: ')
    names = []
    for i in range(400):
        x = 0
        while x < len(mdatabase["genres"][i]):
            if genre in mdatabase["genres"][i][x]:
                names.append(mdatabase["name"][i])
                break
            x += 1
    print('\nThe movies in the genre of', genre, 'are:', '\n')
    for j in range(len(names)):
        print(names[j])     
    
#genre_search()
    
