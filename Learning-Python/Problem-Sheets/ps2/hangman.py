# Problem Set 2, hangman.py
# Name: 
# Collaborators:
# Time spent:

# Hangman Game
# -----------------------------------
# Helper code
# You don't need to understand this helper code,
# but you will have to know how to use the functions
# (so be sure to read the docstrings!)
import random
import string

WORDLIST_FILENAME = "words.txt"


def load_words():
    """
    Returns a list of valid words. Words are strings of lowercase letters.
    
    Depending on the size of the word list, this function may
    take a while to finish.
    """
    print("Loading word list from file...")
    # inFile: file
    inFile = open(WORDLIST_FILENAME, 'r')
    # line: string
    line = inFile.readline()
    # wordlist: list of strings
    wordlist = line.split()
    print(len(wordlist), "words loaded. \n")
    return wordlist



def choose_word(wordlist):
    """
    wordlist (list): list of words (strings)
    
    Returns a word from wordlist at random
    """
    return random.choice(wordlist)

# end of helper code

# -----------------------------------

# Load the list of words into the variable wordlist
# so that it can be accessed from anywhere in the program
wordlist = load_words()


def is_word_guessed(secret_word, letters_guessed):
    '''
    secret_word: string, the word the user is guessing; assumes all letters are
      lowercase
    letters_guessed: list (of letters), which letters have been guessed so far;
      assumes that all letters are lowercase
    returns: boolean, True if all the letters of secret_word are in letters_guessed;
      False otherwise
    '''
    
    for x in secret_word: # run through every letter in secret_word
        if x not in letters_guessed: # check if said letters are in letters_guessed
            return False # if so set boolean variable to false
   
    return True


def get_guessed_word(secret_word, letters_guessed):
    '''
    secret_word: string, the word the user is guessing
    letters_guessed: list (of letters), which letters have been guessed so far
    returns: string, comprised of letters, underscores (_), and spaces that represents
      which letters in secret_word have been guessed so far.
    '''
    
    letters_in_sw = list(secret_word) # put letters in secret word into a list
    
    current_guess = []
    
    for i in range(len(secret_word)):
        current_guess.append('_ ') # create a list with '_ ' replacing each letter in secret word
    
    for x in letters_guessed: # test each letter guessed
        for j in range(len(secret_word)): # run through each letter in the secret word
            if x == letters_in_sw[j]: # check if current letter being checked is correct
                current_guess[j] = x # if so replace the '_ ' with the correct guess
    
    current_guess_str = ''.join(current_guess) # turn this list into a string

    return(current_guess_str)
              

def get_available_letters(letters_guessed):
    '''
    letters_guessed: list (of letters), which letters have been guessed so far
    returns: string (of letters), comprised of letters that represents which letters have not
      yet been guessed.
    '''
    
    available_letters = [] # create empty list of available letters
    
    for x in string.ascii_lowercase: # go through letters in alphabet
        if x not in letters_guessed: # if they haven't been guess, put them in list
            available_letters.append(x)
    
    available_letters_str = ''.join(available_letters) # join entries of list into a string
    
    return available_letters_str


# creating another helper function to calculate the number of unique letts in secret_word
def number_unique_letters(secret_word): 
    
    unique_secret_word = [] 
    
    for x in secret_word: # go through each letter in the string secret_word
        if x not in unique_secret_word: # append to list only if it's not already in there
            unique_secret_word.append(x)
    
    return(len(unique_secret_word)) # return number of unique letters in secret_word

def hangman(secret_word):
    '''
    secret_word: string, the secret word to guess.
    
    Starts up an interactive game of Hangman.
    
    * At the start of the game, let the user know how many 
      letters the secret_word contains and how many guesses s/he starts with.
      
    * The user should start with 6 guesses

    * Before each round, you should display to the user how many guesses
      s/he has left and the letters that the user has not yet guessed.
    
    * Ask the user to supply one guess per round. Remember to make
      sure that the user puts in a letter!
    
    * The user should receive feedback immediately after each guess 
      about whether their guess appears in the computer's word.

    * After each guess, you should display to the user the 
      partially guessed word so far.
    
    Follows the other limitations detailed in the problem write-up.
    '''
    # begin with an opening message
    print('Welcome to the game Hangman! \nI am thinking of a word that is', len(secret_word), 'letters long \n\n~~~~~~~~~~~~~~~~~~~~~~~~~\n')
    
    # setting up initial values and an empty list for letters guessed
    guesses_left = 6
    warnings_left = 3
    letters_guessed = []
    
    while guesses_left > 0:
        
        # calculate total score so far based on changes made to guesses_left in previous iteration
        total_score = guesses_left*number_unique_letters(secret_word)
        
        won = False # set up a boolean value to check at end if game lost
        
        # check if the letters guessed so far mean the user has won before running loop
        if is_word_guessed(secret_word, letters_guessed) == True: # i.e. if guessed correctly
            print('Congratulations, you won!\nYour total score for this game is:', total_score)
            won = True # change the boolean value so loser message doesn't show at end
            break # exit while loop if won
        
        # print out how many guesses are left
        if guesses_left == 1: # if statement so it says '1 guess' not '1 guesses'
            print('You have', guesses_left, 'guess left.')
        else:
            print('You have', guesses_left, 'guesses left.')
        
        # print available letters
        print('Available letters:', get_available_letters(letters_guessed))
        
        guess = str(input('Please enter a letter: ')) # get guess and read as a string
        guess = str.lower(guess) # read input as lower case
        
        # overall structure is 4 if/elif/else statements: 2 to determine if input is valid and
        # give warnings if so, then 2 to determine if guess was correct or not:
        
        # check if guess is a letter and if not, use up one warning and print a message to user
        if str.isalpha(guess) == False: # i.e. if guess not a letter
            warnings_left -= 1 
            if warnings_left < 1: # once the number of warnings drops below 1 start to reduce guesses
                guesses_left -= 1
                print('Oops! That is not a valid letter. You are now out of warnings so will lose a guess:', get_guessed_word(secret_word, letters_guessed), '\n\n~~~~~~~~~~~~~~~~~~~~~~~~~\n')
            else: # otherwise print out number of warnings left
                print('Oops! That is not a valid letter. You have', warnings_left, 'warnings left:', get_guessed_word(secret_word, letters_guessed), '\n\n~~~~~~~~~~~~~~~~~~~~~~~~~\n')
        
        # check if guess is a repeat, if so use up a warning and do the same as if statement above
        elif guess in letters_guessed: # i.e. if user has already guessed this letter
            warnings_left -= 1
            if warnings_left < 1:
                guesses_left -= 1
                print('Oops! You\'ve already guessed that letter. You are now out of warnings so will lose a guess.', get_guessed_word(secret_word, letters_guessed), '\n\n~~~~~~~~~~~~~~~~~~~~~~~~~\n')
            else: 
                print('Oops! You\'ve already guessed that letter. You have', warnings_left, 'warnings left:', get_guessed_word(secret_word, letters_guessed), '\n\n~~~~~~~~~~~~~~~~~~~~~~~~~\n') 
        
        # check if their guess was correct, print out current progress of guesses
        elif guess in secret_word:
            letters_guessed.append(guess) # add guess to letters_guessed list
            print('Good guess:', get_guessed_word(secret_word, letters_guessed), '\n\n~~~~~~~~~~~~~~~~~~~~~~~~~\n')
        
        # for when guess not in secret_word
        else:
            letters_guessed.append(guess)
            print('Oops! That letter is not in my word:', get_guessed_word(secret_word, letters_guessed), '\n\n~~~~~~~~~~~~~~~~~~~~~~~~~\n')
            if guess in 'aeiou': # take away 2 guesses if an incorrect vowel
                guesses_left -= 2
            else: # only take away 1 guess if an incorrect consonant
                guesses_left -= 1
    
    if won == False: # outside of while loop, check boolean value and print losing message
        print('Sorry, you ran out of guesses. The word was:', secret_word)


# When you've completed your hangman function, scroll down to the bottom
# of the file and uncomment the first two lines to test
#(hint: you might want to pick your own
# secret_word while you're doing your own testing)


# -----------------------------------



def match_with_gaps(my_word, other_word):
    '''
    my_word: string with _ characters, current guess of secret word
    other_word: string, regular English word
    returns: boolean, True if all the actual letters of my_word match the 
        corresponding letters of other_word, or the letter is the special symbol
        _ , and my_word and other_word are of the same length;
        False otherwise: 
    '''
     
    my_word = my_word.replace(' ', '') # get rid of whitespace in my_word
    
    if len(my_word) != len(other_word): # if lengths not the same, return false and exit function
        return False
    
    for i in range(len(my_word)): # go through each letter of my_word
        if my_word[i] == '_': # if there is an _ in a position, this letter hasn't been guessed
            continue # so continue on, back to the top of the for loop and check the other letters
        elif my_word[i] == other_word[i]: # if match, check they have the same number of occurences
            if my_word.count(my_word[i]) != other_word.count(other_word[i]):
                return False
            else: # otherwise that letter appears the same number of times in both, so continue
                continue
        else: # if they don't, return False and exit function
            return False
    
    return True 


def show_possible_matches(my_word):
    '''
    my_word: string with _ characters, current guess of secret word
    returns: nothing, but should print out every word in wordlist that matches my_word
             Keep in mind that in hangman when a letter is guessed, all the positions
             at which that letter occurs in the secret word are revealed.
             Therefore, the hidden letter(_ ) cannot be one of the letters in the word
             that has already been revealed.

    '''
    possible_matches = [] # set up a list to store the possible matches
    
    for x in wordlist: # go through each possible secret word
        if match_with_gaps(my_word, x) == True:
            possible_matches.append(x) # fill list with words which are possible solutions
    
    possible_matches_str = ' '.join(possible_matches) # turn list into a string and return it
    
    return(possible_matches_str)
    


def hangman_with_hints(secret_word):
    '''
    secret_word: string, the secret word to guess.
    
    Starts up an interactive game of Hangman.
    
    * At the start of the game, let the user know how many 
      letters the secret_word contains and how many guesses s/he starts with.
      
    * The user should start with 6 guesses
    
    * Before each round, you should display to the user how many guesses
      s/he has left and the letters that the user has not yet guessed.
    
    * Ask the user to supply one guess per round. Make sure to check that the user guesses a letter
      
    * The user should receive feedback immediately after each guess 
      about whether their guess appears in the computer's word.

    * After each guess, you should display to the user the 
      partially guessed word so far.
      
    * If the guess is the symbol *, print out all words in wordlist that
      matches the current guessed word. 
    
    Follows the other limitations detailed in the problem write-up.
    '''
    
    # most of this code is the same as the hangman function above so I have removed the comments 
    # to make it easier to read, apart from new bits which I have commented

    print('Welcome to the game Hangman! \nI am thinking of a word that is', len(secret_word), 'letters long \n\n~~~~~~~~~~~~~~~~~~~~~~~~~\n')
    
    guesses_left = 6
    warnings_left = 3
    letters_guessed = []
    
    while guesses_left > 0:
        
        total_score = guesses_left*number_unique_letters(secret_word)
        
        won = False 
        
        if is_word_guessed(secret_word, letters_guessed) == True: 
            print('Congratulations, you won!\nYour total score for this game is:', total_score)
            won = True
            break
        
        if guesses_left == 1: 
            print('You have', guesses_left, 'guess left.')
        else:
            print('You have', guesses_left, 'guesses left.')
        
        print('Available letters:', get_available_letters(letters_guessed))
        
        guess = str(input('Please enter a letter: ')) 
        guess = str.lower(guess)
        
        # the following if statement before the others, turning the next if statement into an elif
        # is the only addition I've made. This checks if the user has input a * and if they
        # have it displays all possible words for them
        if guess == '*':
            print('Possible word matches for', get_guessed_word(secret_word, letters_guessed), 'are:\n\n', show_possible_matches(get_guessed_word(secret_word, letters_guessed)), '\n\n~~~~~~~~~~~~~~~~~~~~~~~~~\n')
                
        elif str.isalpha(guess) == False: 
            warnings_left -= 1 
            if warnings_left < 1: 
                guesses_left -= 1
                print('Oops! That is not a valid letter. You are now out of warnings so will lose a guess:', get_guessed_word(secret_word, letters_guessed), '\n\n~~~~~~~~~~~~~~~~~~~~~~~~~\n')
            else: 
                print('Oops! That is not a valid letter. You have', warnings_left, 'warnings left:', get_guessed_word(secret_word, letters_guessed), '\n\n~~~~~~~~~~~~~~~~~~~~~~~~~\n')
        
        elif guess in letters_guessed: 
            warnings_left -= 1
            if warnings_left < 1:
                guesses_left -= 1
                print('Oops! You\'ve already guessed that letter. You are now out of warnings so will lose a guess.', get_guessed_word(secret_word, letters_guessed), '\n\n~~~~~~~~~~~~~~~~~~~~~~~~~\n')
            else: 
                print('Oops! You\'ve already guessed that letter. You have', warnings_left, 'warnings left:', get_guessed_word(secret_word, letters_guessed), '\n\n~~~~~~~~~~~~~~~~~~~~~~~~~\n') 
        
        elif guess in secret_word:
            letters_guessed.append(guess) 
            print('Good guess:', get_guessed_word(secret_word, letters_guessed), '\n\n~~~~~~~~~~~~~~~~~~~~~~~~~\n')
        
        else:
            letters_guessed.append(guess)
            print('Oops! That letter is not in my word:', get_guessed_word(secret_word, letters_guessed), '\n\n~~~~~~~~~~~~~~~~~~~~~~~~~\n')
            if guess in 'aeiou':
                guesses_left -= 2
            else:
                guesses_left -= 1
    
    if won == False: 
        print('Sorry, you ran out of guesses. The word was:', secret_word)




# When you've completed your hangman_with_hint function, comment the two similar
# lines above that were used to run the hangman function, and then uncomment
# these two lines and run this file to test!
# Hint: You might want to pick your own secret_word while you're testing.


if __name__ == "__main__":
    # pass

    # To test part 2, comment out the pass line above and
    # uncomment the following two lines.
    
    #secret_word = choose_word(wordlist)
    #hangman(secret_word)
    
###############
    
    # To test part 3 re-comment out the above lines and 
    # uncomment the following two lines. 
    
    secret_word = choose_word(wordlist)
    hangman_with_hints(secret_word)
