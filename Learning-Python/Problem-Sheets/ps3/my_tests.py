""" Going back to ps3 solution a while after I wrote it and writing tests for it without looking at test_ps3.py """

import unittest
import ps3 as hangman


class testfunc(unittest.TestCase):


    def setUp(self):
        print("setting up")


    def tearDown(setUp):
        print()


    def test_get_word_score(self):
        print('running test to check word score is calculated correctly')

        word = 'hello'  # does it matter that I use the same variable names as in the actual function?
        score = 232
        n = 7
        try:
            self.assertEqual(score, hangman.get_word_score(word, n))
        except AssertionError:
            print('FAILED when testing get_word_score with correct answer')

        word = 'hello'
        score = 231
        n = 7
        try:
            self.assertNotEqual(score, hangman.get_word_score(word, n))
        except AssertionError:
            print('FAILED when testing get_word_score with incorrect answer')

        word = 'HeLLO'
        score = 232
        n = 7
        try:
            self.assertEqual(score, hangman.get_word_score(word, n))
        except AssertionError:
            print('FAILED when testing get_word_score with capital and lowercase letters')

        word = 'h*llo'
        score = 203
        n = 7
        try:
            self.assertEqual(score, hangman.get_word_score(word, n))
        except AssertionError:
            print('FAILED when testing get_word_score with wildcard')


    def test_update_hand(self):
        print('running test to check hand is updated correctly')

        hand = {'t': 2, 'e': 1, 's': 2, '*': 1, 'y': 1}
        word = 'test'
        updated_hand = {'t': 0, 'e': 0, 's': 1, '*': 1, 'y': 1}
        try:
            self.assertEqual(updated_hand, hangman.update_hand(hand, word))
        except AssertionError:
            print('FAILED when testing update_hand with correct answer')

        hand = {'t': 2, 'e': 1, 's': 2, '*': 1, 'y': 1}
        word = 'test'
        updated_hand = {'t': 1, 'e': 0, 's': 1, '*': 1, 'y': 1}
        try:
            self.assertNotEqual(updated_hand, hangman.update_hand(hand, word))
        except AssertionError:
            print('FAILED when testing update_hand with incorrect answer')

        hand = {'t': 2, 'e': 1, 's': 2, '*': 1, 'y': 1}
        word = 'TEst'
        updated_hand = {'t': 0, 'e': 0, 's': 1, '*': 1, 'y': 1}
        try:
            self.assertEqual(updated_hand, hangman.update_hand(hand, word))
        except AssertionError:
            print('FAILED when testing update_hand with capital and lowercase letters')

        hand = {'t': 2, 'e': 1, 's': 2, '*': 1, 'y': 1}
        word = 'T*st'
        updated_hand = {'t': 0, 'e': 1, 's': 1, '*': 0, 'y': 1}
        try:
            self.assertEqual(updated_hand, hangman.update_hand(hand, word))
        except AssertionError:
            print('FAILED when testing update_hand with wildcard')


    def test_is_valid_word(self):
        print('running test to check if words are accepted and rejected correctly')

        word_list = hangman.load_words()

        hand = {'t': 2, 'e': 1, 's': 2, '*': 1, 'y': 1}
        word = 'test'
        try:
            self.assertTrue(hangman.is_valid_word(word, hand, word_list))
        except AssertionError:
            print('FAILED when testing is_valid_word with correct answer')

        hand = {'t': 2, 'e': 1, 's': 2, '*': 1, 'y': 1}
        word = 'testt'
        try:
            self.assertFalse(hangman.is_valid_word(word, hand, word_list))
        except AssertionError:
            print('FAILED when testing is_valid_word with incorrect answer: letter not in hand')

        hand = {'t': 2, 'e': 1, 's': 2, '*': 1, 'y': 1}
        word = 'tesy'
        try:
            self.assertFalse(hangman.is_valid_word(word, hand, word_list))
        except AssertionError:
            print('FAILED when testing is_valid_word with incorrect answer: not a real word')

        hand = {'t': 2, 'e': 1, 's': 2, '*': 1, 'y': 1}
        word = 'teST'
        try:
            self.assertTrue(hangman.is_valid_word(word, hand, word_list))
        except AssertionError:
            print('FAILED when testing is_valid_word with capital and lowercase letters')

        hand = {'t': 2, 'e': 1, 's': 2, '*': 1, 'y': 1}
        word = 't*st'
        try:
            self.assertTrue(hangman.is_valid_word(word, hand, word_list))
        except AssertionError:
            print('FAILED when testing is_valid_word with wildcard')


if __name__=='__main__':
    unittest.main()  # invokes runner on the extended class