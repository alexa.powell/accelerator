const Gameboard = require('./Gameboard');
const Ship = require('./Battleship');

describe('testing Gameboard factory function', () => {
    let gameboard;
    let shipName;
    beforeEach(() => {
        gameboard = Gameboard(10);
    });

    test('board should be an object', () => {
        expect(gameboard).toBeDefined();
    });

    test('board should be right size', () => {
        expect(gameboard.board.length).toEqual(10);
        expect(gameboard.board[0].length).toEqual(10);
    });

    test('should initiate shipSunkStatuses', () => {
        expect(gameboard.shipSunkStatuses).toBeDefined();
    });

    test('should populate an object of statuses with false (i.e. not sunk)', () => {
        gameboard.populateShipList(shipName);
        expect(gameboard.shipSunkStatuses[shipName]).toBeFalsy;
    });

    test('should create a ship with name fed into PlaceShip', () => {
        let testShip = gameboard.placeShip(shipName, 0, 0, 4, 'h');
        expect(testShip).toBeDefined();
        expect(testShip.length).toEqual(4);
    });

    test('placeShip() should fill in list of ship sunk statuses with initial value false', () => {
        gameboard.placeShip(shipName, 0, 0, 4, 'h');
        expect(gameboard.shipSunkStatuses[shipName]).toBeFalsy();
    });

    test('should initiate shipPositionList', () => {
        expect(gameboard.shipPositionList).toBeDefined();
    });

    test('populatePositionList() should work correctly with horizontal orientation', () => {
        gameboard.populatePositionList(shipName, 0, 0, 2, 'h');
        let testPositionList = [{ship: shipName, x: 0, y: 0, position: 0}, {ship: shipName, x: 0, y: 1, position: 1}];
        expect(gameboard.shipPositionList).toEqual(testPositionList);
    });

    test('populatePositionList() should work correctly with vertical orientation', () => {
        gameboard.populatePositionList(shipName, 1, 1, 3, 'v');
        let testPositionList = [{ship: shipName, x: 1, y: 1, position: 0}, {ship: shipName, x: 2, y: 1, position: 1}, {ship: shipName, x: 3, y: 1, position: 2}];
        expect(gameboard.shipPositionList).toEqual(testPositionList);
    });

    // this was when I had a beforeAll function but I combatted this by chaging it to a beforeEach
    // test('check shipPositionList is emptied for testing inside placeShip function', () => {
    //     gameboard.shipPositionList.length = 0;
    //     expect(gameboard.shipPositionList[0]).not.toBeDefined;
    // });

    test('calling placeShip should correctly implement populatePositionList', () => {
        gameboard.placeShip(shipName, 0, 0, 2, 'h');
        testShipName = Ship(2);
        let testPositionList = [{ship: testShipName, x: 0, y: 0, position: 0}, {ship: testShipName, x: 0, y: 1, position: 1}];
        for (i = 0; i < 2; i++) {
            expect(gameboard.shipPositionList[i].position).toEqual(testPositionList[i].position);
            expect(gameboard.shipPositionList[i].x).toEqual(testPositionList[i].x);
            expect(gameboard.shipPositionList[i].y).toEqual(testPositionList[i].y);
            expect(gameboard.shipPositionList[i].ship.hits).toEqual(testPositionList[i].ship.hits);
            expect(gameboard.shipPositionList[i].ship.length).toEqual(testPositionList[i].ship.length);
        }
    });

    test('placeShip should throw errors when given invalid input 2', () => {
        let OrientationError = Error("orientation not valid, input 'h' or 'v'");
        let CoordinateError = Error("coordinates not on board");
        expect(() => {gameboard.placeShip(shipName, 0, 0, 2, 'p')}).toThrow(OrientationError);
        expect(() => {gameboard.placeShip(shipName, -1, 0, 2, 'h')}).toThrow(CoordinateError);
        expect(() => {gameboard.placeShip(shipName, 0, 10, 2, 'h')}).toThrow(CoordinateError);
    });

    test('updateBoardWithShip should add ships being placed to the board', () => {
        gameboard.updateBoardWithShip(4, 4);
        expect(gameboard.board[4][4].ship).toBeTruthy();
    });

    test('^ should still as expected when called in populateShipList in placeShip', () => {
        gameboard.placeShip(shipName, 0, 0, 4, 'h');
        for (i = 0; i < 4; i++) {
            let boardPosition = gameboard.board[0][i];
            expect(boardPosition.ship).toBeTruthy();
            expect(boardPosition.hit).toBeFalsy();
        } 
    });
    
    test('didAttackHitShip should know whether a ship has been hit or not', () => {
        gameboard.placeShip(shipName, 0, 0, 4, 'h');
        expect(gameboard.didAttackHitShip(0, 1)).toBeTruthy();
        expect(gameboard.didAttackHitShip(1, 0)).toBeFalsy();
    });

    test('implementAttack should update the ship array hits', () => {
        let ship = gameboard.placeShip(shipName, 0, 0, 4, 'h');
        gameboard.implementAttack(0, 0);
        expect(ship.hits[0]).toBeTruthy();
    });

    // test to check board has been updated
});