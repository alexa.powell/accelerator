
describe('chillgametest', () => {
    let listUnderTest = null;
    
    beforeEach(() => {
        desiredOutput = new chillgametest();
    });

    describe('on opening the page', () => {
        it('should resize the game appropriately', () => {
            expect(desiredOutput.resize()).to.equal('landscape');
        });
        
        it('should return the correct array of coordinates for sin wave', () => {
            expect(desiredOutput.sinPattern(120, 40, 64)).to.have.length(64);
        });
    });
});